<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Users;
use Webpatser\Uuid\Uuid;
use App\Model\Order;

class Website extends Controller
{
    
    // public function view($name){
    //     return  view('youtube',['data'=>$name]);
    // }
    // public function site(Request $request){
    //     print_r($request->path());
    // }
    // public function index(Request $request){
    //     // if($request->isMethod('get')){
    //     //     echo "get request";
    //     // }
    //     echo $request->input('name');
    // }
    public function login(Request $request)
    {
        $request->validate([
            'username'=>'required|min:3',
            'password'=>'required|min:6|max:10'
        ]);
        

        print_r($request->input());

        $request->session()->flash('username', $request->input('username'));
        return redirect('welcome');
    }

    public function blade()
    {
        $data = ['name'=>'Phuong',
                'age'=>'20',
                'cmt'=>'036099007472',
                'sex'=>'nam',
                'height'=>'<h1>1m6</h1>'];
        return view('blade', ['items'=>$data]);
    }
    public function db()
    {
        $result = DB::select('select * from users');
        // print_r ($result);
        $data = DB::table('users')
        // ->join('news','news.user_id','=','users.id')
        ->get()
        ->toArray();
        echo "<pre>";
        print_r($data);
    }
    public function model()
    {
        return User::All();
    }

    public function view()
    {
        $data = DB::table('users')->paginate(1);
        return view('view', ['data' => $data]);
    }

    public function test()
    {
        $data = Users::GetUsers();
        print_r($data->toArray());
    }

    public function uuid()
    {
        // return "hello";
        print_r(Uuid::generate());
    }

    public function api()
    {
        // echo "some data";
        $data = User::all();    
        return $data;
    }

    public function insert(request $request)
    {
        // print_r($request->input());
        $order = new Order;
        $order->id = $request->input('id');
        $order->OrderNumber = $request->input('OrderNumber');
        $order->user_id = $request->input('user_id');
        $order->save();
    }
}
