<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //
    public function scopeGetUsers($query){
        return $query->get();
    }
}
