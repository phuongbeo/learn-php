<!DOCTYPE html>
<html lang="en">
<head>
	<title>Quản lý sinh viên</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
	<ul class="breadcrumb">
		<li class="active" id="home_menu">Home</li>
		<li id="user_managerment_menu">User managerment</li>
		<li id="add_user_menu">Add user</li>
	</ul>
	<div class="container-fluid" id="user_managerment_panel">
		<div class="panel panel-primary">
			<div class="panel-heading">User managerent</div>
			<div class="panel-body">
				<table class="table">
					<thead>
						<tr>
							<td>No</td>
							<td>Name</td>
							<td>Email</td>
							<td>Created at</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody id="dataList">
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="container-fluid" id="add_user_panel">
		<div class="panel panel-primary">
			<div class="panel-heading">Add user</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<form id="form-create" action="/api/user" class="form" method="POST" onsubmit="submitForm(); return false;">
							<div class="form-group">
								<label>Name:</label>
								<input type="text" name="name" class="form-control" placeholder="fullname" required>
							</div>
							<div class="form-group">
								<label>Email:</label>
								<input type="email" name="email" class="form-control" placeholder="email" required>
							</div>
							<div class="form-group">
								<label>Password:</label>
								<input type="password" name="password" class="form-control" placeholder="Password" required>
							</div>

							<div id="errors"></div>

							<button type="submit" class="btn btn-primary">Add user</button>
							<button type="reset" class="btn btn-default">Reset</button>				
						</form>
					</div>
					<div class="col-md-6">
						<form id="form-edit" action="" class="form" method="POST" onsubmit="submitFormEdit(); return false;">
							<div class="form-group">
								<label>Name:</label>
								<input type="text" id="edit_name" name="name" class="form-control" placeholder="fullname" required>
							</div>
							<div class="form-group">
								<label>Email:</label>
								<input type="email" id="edit_email" name="email" class="form-control" placeholder="email" required>
							</div>
							<div id="errors_edit"></div>

							<button type="submit" class="btn btn-primary">Edit user</button>
							<button type="reset" class="btn btn-default">Reset</button>				
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function(){
			// $("input").val("123"); //tag name
			// $("input:first").val("123"); //tag first name
			// $("input:last").val("123");  //tag last name
			// $("#id_fullname").val("nguyễn thanh phương"); //using id
			// $(".c_fullname").val("phương béo"); //using class
			// $("[name = fullname]").val("hello world"); //using other properties
			// alert($("[name = fullname]").val()); //chi sử dụng trên thẻ input
			// $("input.c_fullname").val("hello"); //goi input xong goi den class
			// $(":text").val("abc"); //thao tác lên type:text
			// $(".breadcrumb").append("<li>contact</li>"); //thêm vào sau
			// $(".breadcrumb").prepend("<li>contact</li>"); //thêm vào trước
			// $(".breadcrumb").before("<li>contact</li>"); //cùng cấp ul nhưng thêm bên trên
			// $(".breadcrumb").after("<li>contact</li>"); //cùng cấp ul nhưng thêm bên duoi

			loadUsers();

			var no = 0;
			$("#home_menu").click(function(){
				$("#user_managerment_panel").show();
				$("#add_user_panel").show();
			});

			$("#user_managerment_menu").click(function(){
				$("#user_managerment_panel").show();
				$("#add_user_panel").hide();
			});

			$("#add_user_menu").click(function(){
				$("#user_managerment_panel").hide();
				$("#add_user_panel").show();
			});
		});

		function appendUser(user) {
			$("#dataList").append("<tr class=\"user_row\" data-id=\"" + user.id + "\">"+
				"<td>"+user.id+"</td>"+
				"<td>"+user.name+"</td>"+
				"<td>"+user.email+"</td>"+
				"<td>"+user.created_at+"</td>"+
				'<td><button type="button" class="btn edit btn-info">Edit</button><button type="button" class="btn delete btn-delete">Delete</button></td>'+
				"</tr>");
		}

		function submitForm() {
			var form = $('#form-create');
			$('#errors').empty();
			$.ajax({
				type: "POST",
				url: form.attr('action'),
				data: form.serialize(),
				cache: false,
				success: function(data) {
					if (data.success) {
						appendUser(data.data);
						form.trigger("reset");	
					} else {
						for (var k in data.errors) {
							$('#errors').append("<p class=\"text-danger\">" + data.errors[k][0] + "</p>");
						}
					}
				}
			});
		}

		function registerButtonEvent() {
			// edit
			$('.edit').on('click', function(e) {
				var row = e.currentTarget;
				var id = $(row).parent().parent().data('id');
				var attr = "/api/user/" + id;
				
				// call api show
				$.ajax({
					url: attr,
					cache: false,	
					success: function(data) {
						$('#form-edit').attr('action', attr);
						$('#edit_name').val(data.name).focus();
						$('#edit_email').val(data.email);
					}
				});
			});

			// delete
			$('.delete').on('click', function(e) {
				if (!confirm('Want to delete?')) return;

				var row = e.currentTarget;
				var id = $(row).parent().parent().data('id');
				var attr = "/api/user/" + id;
				
				// call api show
				$.ajax({
					type: "DELETE",
					url: attr,
					cache: false,	
					success: function(data) {
						if (data.success) {
							$('.user_row[data-id='+id+']').remove();
						} else {
							alert('Không tồn tại user');
						}
					}
				});
			});
		}

		function submitFormEdit() {
			var form = $('#form-edit');
			$('#errors_edit').empty();
			$.ajax({
				type: "PUT",
				url: form.attr('action'),
				data: form.serialize(),
				cache: false,
				success: function(data) {
					if (data.success) {
						loadUsers();
						form.trigger("reset");
					} else {
						for (var k in data.errors) {
							$('#errors_edit').append("<p class=\"text-danger\">" + data.errors[k][0] + "</p>");
						}
					}
				}
			});
		}

		function loadUsers () {
			$('#dataList').empty();
			$.ajax({
				url: "/api/user",
				cache: false,
				success: function(data) {

					for (var i = 0; i < data.length; i++) {
						appendUser(data[i]);
					}

					registerButtonEvent();
				}
			});
		}
	</script>
</body>
</html>