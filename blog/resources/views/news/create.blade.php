@extends('layouts.app')

@push('styles')
    <style>
        
    </style>
@endpush

@section('content')
    <form action="/news" method="POST">
        @csrf
        <label for="">Title</label> <br>
        <input type="text" name="title" required> <br>
        <label for="">Content</label> <br>
        <textarea name="content" id="" cols="30" rows="10" required></textarea> <br>
        <button>Create</button>
    </form>
@endsection

@push('scripts')
    
@endpush
