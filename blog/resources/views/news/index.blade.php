@extends('layouts.app')

@push('styles')
<style>

</style>
@endpush

@section('content')
<h1>List:</h1>
<p style="text-align: right">
    <a href="/news/create">Tao bai viet</a>
</p>

@foreach($listNews as $news)
<h4>
    <a href="/news/{{ $news->id }}">
        {{ $news->title }}
    </a>
    <a href="/news/{{ $news->id }}/edit"><small>Edit</small></a>
    <form action="/news/{{ $news->id }}" method="POST">
        @csrf
        @method('DELETE')
        <button type="submit" onclick="return confirm('Want to delete?');">Delete</button>
    </form>
</h4>
@endforeach
@endsection

@push('scripts')
<script>
    
</script>
@endpush