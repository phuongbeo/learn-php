@extends('layouts.app')

@push('styles')
<style>

</style>
@endpush

@section('content')
<h4>
    {{ $news->title }}
</h4>
<p>{{ $news->content }}</p>                                                 
@endsection

@push('scripts')

@endpush