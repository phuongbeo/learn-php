<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/account', 'HomeController@account')->name('my_account');

// Route::get('first', function () {
//     return view('first');
// });
// Route::get('/{name}', function ($name) {
//     return $name;
// });
// Route::get('/user/{name?}', function ($name="hello") {
//     return $name;
// });

// Route::get('/view/{name}','Website@view');
// Route::get('/site', 'Website@site');
// route::any('/index','Website@index');

 Route::get('/login', function () {
     return view('login');
 });

 Route::post('/login', 'Website@login')->middleware('checkage');

Route::view('welcome', 'welcome'); //làm session
Route::view('form', 'login');
// Route::post('login', 'Website@login');

Route::resource('news', 'NewsController');

Route::get('blade', 'Website@blade');

Route::view('/page', 'page');
Route::view('/page2', 'page2');

Route::get('/localization/{lang}', function ($lang) {
    App::setlocale($lang);
    return view('localization');
});

Route::get('/db', 'Website@db');

Route::get('/model', 'Website@model');

Route::get('/view', 'Website@view'); // Chiếu đơn giản (view.blade.php +Website.php)

Route::get('/test', 'Website@test'); //phạm vi trong mô hình laravel (Users.php + Website.php)

Route::get('/relation', 'Test@up'); //quan hệ một đối một | mối quan hệ trong laravel (test.php + User.php)

Route::get('/relation2', 'Test@next');//một đến nhiều mối quan hệ | mối quan hệ trong laravel (nhu tren)

Route::view('file', 'file'); //file upload (file.blade.php + youtube.php)
Route::post('upload', 'Youtube@index');

Route::view('ajax', 'ajax');

Route::get('/test', 'testController@index');
