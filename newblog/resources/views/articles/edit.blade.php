@extends('layouts.master')

@section('head.title')
    Chỉnh sửa nội dung
@stop
@section('body.content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h1>Cập nhật nội dung</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                @foreach($errors->all() as $error)
                    <li style="color: red">{{ $error }}</li>
                @endforeach
                <form action="{{ route('article.update', $article) }}" method="POST" class="form-horizontal">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title" class="control-label">Tiêu đề bài viết</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="Tiêu đề bài viết"
                               required value="{{ $article->title }}">
                    </div>
                    <div class="form-group">
                        <label for="content" class="control-label">Nội dung bài bài viết</label>
                        <input type="text" name="content" class="form-control" id="content"
                               placeholder="Nội dung bài viết" required value="{{ $article->content }}">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Cập nhật bài viết</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
