<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blogger Phương Nguyễn</title>
    <link rel="stylesheet" href="{{ asset_custom('css/bootstrap.min.css') }}">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    @stack('styles')
</head>
<body style=" font-family: Tahoma;">
<div class="container">
    @yield('content')
</div>
<script src="{{ asset_custom('js/jquery.min.js') }}"></script>
<script src="{{ asset_custom('js/bootstrap.min.js') }}"></script>
@stack('scripts')
</body>
</html>
