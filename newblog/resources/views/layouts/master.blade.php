<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('head.title')</title>
    <link rel="stylesheet" href="{{ asset_custom('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="/css/style.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    @yield('head.css')
</head>
<body>
@include('partials.navbar')
@yield('body.content')
@include('partials.footer')

<script src="{{ asset_custom('js/jquery.min.js') }}"></script>
<script src="{{ asset_custom('js/bootstrap.min.js') }}"></script>
@yield('body.js')
</body>
</html>