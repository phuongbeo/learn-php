<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/phuong-blog','PhuongBlogController@index')->name('phuong_blog');
Route::get('/test', 'SummernoteController@index');
Route::name('article.')->namespace('Admin')->group(function () {
    Route::resource('articles', 'ArticlesController')->names([
        'index' => 'index',
        'show' => 'show',
        'create' => 'create',
        'store' => 'store',
        'edit' => 'edit',
        'update' => 'update',
        'destroy' => 'destroy'
    ]);
});

Auth::routes();
