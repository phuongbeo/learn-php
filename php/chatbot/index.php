<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chatbot</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <?php
            // kết nối CSDL
            $link = mysqli_connect('localhost','root','');
            // chọn CSDL
            mysqli_select_db($link,'chatbot');
            if(isset($_POST['add'])){
                $user = $_POST['user'];
                $comment = $_POST['comment'];

                // chèn vào CSDL
                mysqli_query($link,"INSERT INTO comments VALUES('','$user', '$comment')") or die(mysqli_error($link));
            }
            ?>
            <form action="" enctype="multipart/form-data" method="post" role="form">
                <div class="card bg-light text-dark">
                    <div class="card-header">Content</div>
                    <div class="card-body">
                        <div style="width: 500px; height: 300px;border: 1px solid #aaa;overflow: scroll;" name="" id="" cols="30" rows="14">
                            <?php
                            $sql = mysqli_query($link, "SELECT * FROM comments");
                            while ($row = mysqli_fetch_array($sql)){
                                echo '<b class="ml-2">'. $row['user'] .':</b> ' . $row['comment'].'<br>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group">
                            <label for="">User</label>
                            <input type="text" class="form-control" name="user" placeholder="Enter user" required>
                        </div>
                        <div class="form-group">
                            <label for="">Comment</label>
                            <input type="text" class="form-control" name="comment" placeholder="Enter comment" required>
                        </div>
                        <button class="btn btn-info" type="submit" name="add">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>