<?php
require_once 'db_connection.php';

$category_id = $_GET['id'];
$sql = "select * from product where id_list = $category_id";
$result = executeQuery($sql, true);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Sản phẩm adidas ultra boost</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/adidas.css">
</head>
<body>
<div class="container">
    <?php include_once('layout/header.php') ?>
    <div class="row">
        <div class="col-sm-9">
            <div class="row">
                <?php foreach ($result as $key => $u): ?>
                    <div class="col-sm-4">
                        <img src="<?php echo $u['image'] ?>" class="img-rounded" alt="" style="width: 100%;height: 150px">
                        <b><?php echo number_format($u['price']) ?><u>đ</u></b>
                        <p class="text-center"><?php echo $u['name'] ?></p>
                        <a href="detail.php?id=<?php echo $u['id']; ?>&name=<?php echo $u['name']; ?>">
                            <button type="button" class="btn btn-info">Chi tiết sản phẩm</button>
                        </a>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
        <?php include_once('layout/danh-muc.php') ?>
    </div>
    <hr>
    <div class="text-center">
        <p>Copyright &copy; thương hiệu Adidas</p>
    </div>
</div>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>