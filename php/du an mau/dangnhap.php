<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-4">
            <img src="assets/img/dangnhap.jpg" alt="" style="width: 300px;height: 200px;margin-top: 50px">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
           <form action="post-dangnhap.php" method="post" role="form" style="margin-top: 50px">
               <legend class="text-center">Đăng nhập tài khoản</legend>
               <div class="form-group">
                   <label for="">Email</label>
                   <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" required>
               </div>
               <div class="form-group">
                   <label for="">Password</label>
                   <input type="password" class="form-control" name="password" id="password" placeholder="Enter passwoord"
                          required>
                   <br>
                   <?php
                   if (isset($_GET['error'])) {
                       echo "<label class='text-danger'>{$_GET['error']}</label>";
                   }
                   ?>
               </div>
               <div class="form-group">
                   <button type="submit" class="btn btn-danger">Submit</button>
               </div>
           </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>