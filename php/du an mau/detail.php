<?php
require_once 'db_connection.php';

$product_id = $_GET['id'];
$sqlComment = "select * from comment where id_product = $product_id ORDER BY date DESC";
$comments = executeQuery($sqlComment, true);

$sqlProduct = "select * from product where id = $product_id";
$result = executeQuery($sqlProduct, true);
if (count($result) == 0) die();

$product = $result[0];
//var_dump($product);
//exit();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chi tiết sản phẩm</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/adidas.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style>
        .user_name {
            font-size: 14px;
            font-weight: bold;
        }

        .comments-list .media {
            border-bottom: 1px dotted #ccc;
        }
    </style>
</head>
<body>
<div class="container">
    <?php include_once('layout/header.php') ?>
    <div class="row">
        <div class="col-sm-9">
            <div class="row" style="margin-bottom: 20px">
                <div class="col-sm-6 col-sm-offset-3">
                    <h2 class="text-center" style="color: red">Chi tiết sản phẩm</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <img src="<?php echo $product['image']; ?>" alt="" style="width: 100%;height: auto">
                </div>
                <div class="col-sm-6">
                    <h2 style="color: gray"><?php echo $product['name']; ?></h2>
                    <h3>Giá sản phẩm: <span style="color: red"><?php echo number_format($product['price']) ?>
                            <u>đ</u></span></h3>
                    <p><?php echo $product['detail']; ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form action="comments/add-comment.php" method="post" role="form" style="margin-top: 50px">
                        <input type="hidden" name="id_product" value="<?php echo $product_id; ?>">
                        <legend>Đánh giá về sản phẩm</legend>
                        <div class="form-group">
                            <label for="">Name</label><br>
                            <input type="text" name="name" required placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="">Bình luận</label>
                            <textarea name="content" cols="120" rows="5" placeholder="mời bạn viết đánh giá về sản phẩm"
                                      required></textarea>
                            <input type="hidden" value="<?php echo $_SESSION['auth']['username']; ?>" name="user">
                        </div>
                        <button type="submit" class="btn btn-primary">Gửi</button>
                    </form>
                    <div class="comments-list" style="margin-top: 20px">
                        <?php foreach ($comments as $key => $u): ?>
                            <div class="media">
                                <p class="pull-right">
                                    <small><?php echo $u['date']?></small>
                                </p>
                                <a class="media-left" href="#">
                                    <img src="https://ui-avatars.com/api/?name=<?php echo $u['user'] ?>">
                                </a>
                                <div class="media-body">

                                    <h4 class="media-heading user_name"><?php echo $u['user'] ?></h4>
                                    <?php echo $u['content'] ?>

<!--                                    <p>-->
<!--                                        <small><a href="">Like</a> - <a href="">Share</a></small>-->
<!--                                    </p>-->
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 40px">
                <h2 style="color: orangered;margin-bottom: 30px">Sản phẩm liên quan</h2>
                <div class="col-sm-4">
                    <img src="assets/img/ultra1.jpg" class="img-rounded" alt="" style="width: 100%;height: 150px">
                    <b>4,500,000 <u>đ</u></b>
                    <p>Adidas Ultraboost x GOT “Night’s Watch</p>
                    <a href="detail.php">
                        <button type="button" class="btn btn-info">Chi tiết sản phẩm</button>
                    </a>
                </div>
                <div class="col-sm-4">
                    <img src="assets/img/ultra2.jpg" class="img-rounded" alt="" style="width: 100%;height: 150px">
                    <b>2,350,000 <u>đ</u></b>
                    <p>Adidas UltraBoost Parley </p>
                    <a href="detail.php">
                        <button type="button" class="btn btn-info">Chi tiết sản phẩm</button>
                    </a>
                </div>
                <div class="col-sm-4">
                    <img src="assets/img/ultra4.jpg" class="img-rounded" alt="" style="width: 100%;height: 150px">
                    <b>1,500,000 <u>đ</u></b>
                    <p>Adidas UltraBoost Laceless “Ash Pearl”</p>
                    <a href="detail.php">
                        <button type="button" class="btn btn-info">Chi tiết sản phẩm</button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once('layout/danh-muc.php') ?>
    </div>
    <hr>
    <div class="text-center">
        <p>Copyright &copy; thương hiệu Adidas</p>
    </div>
</div>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>