<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Adidas</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/adidas.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <?php include_once('layout/header.php'); ?>
    <div class="row">
        <div class="col-sm-9">
            <?php include_once('layout/slide-show.php'); ?>
            <div class="row" style="margin-top: 30px">
                <h2 style="color: orangered;margin-bottom: 20px">Sản phẩm yêu thích</h2>
                <div class="col-sm-4">
                    <img src="assets/img/ultra1.jpg" class="img-rounded" alt="" style="width: 100%;height: 150px">
                    <b>4,500,000 <u>đ</u></b>
                    <p>Adidas Ultraboost x GOT “Night’s Watch</p>
                    <a href="detail.php">
                        <button type="button" class="btn btn-info">Chi tiết sản phẩm</button>
                    </a>
                </div>
                <div class="col-sm-4">
                    <img src="assets/img/ultra2.jpg" class="img-rounded" alt="" style="width: 100%;height: 150px">
                    <b>2,350,000 <u>đ</u></b>
                    <p>Adidas UltraBoost Parley </p>
                    <a href="detail.php">
                        <button type="button" class="btn btn-info">Chi tiết sản phẩm</button>
                    </a>
                </div>
                <div class="col-sm-4">
                    <img src="assets/img/ultra4.jpg" class="img-rounded" alt="" style="width: 100%;height: 150px">
                    <b>1,500,000 <u>đ</u></b>
                    <p>Adidas UltraBoost Laceless “Ash Pearl”</p>
                    <a href="detail.php">
                        <button type="button" class="btn btn-info">Chi tiết sản phẩm</button>
                    </a>
                </div>
            </div>
        </div>
        <?php include_once('layout/danh-muc.php'); ?>
    </div>
    <hr>
    <div class="text-center">
        <p>Copyright &copy; thương hiệu Adidas</p>
    </div>
</div>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>