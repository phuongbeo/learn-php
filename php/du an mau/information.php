<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/information.css">
<div class="container" style="margin-top: 50px">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <a target="_blank" href="admin.php"><i class="fa fa-arrow-left"></i> Back</a>
            <div class="card hovercard">
                <div class="cardheader">

                </div>
                <div class="avatar">
                    <img alt="" src="assets/app/media/img/users/lock.jpg">
                </div>
                <div class="info">
                    <div class="title">
                        <a target="_blank" href="#">Phương Béo</a>
                    </div>
                    <div class="desc">Developers</div>
                    <div class="desc">Information Technology</div>
                    <div class="desc">FPT Polytechnic</div>
                </div>
                <div class="bottom">
                    <a class="btn btn-primary btn-sm" rel="publisher"
                       href="https://www.facebook.com/star.lolely">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a class="btn btn-danger btn-sm" rel="publisher"
                       href="https://plus.google.com/+ahmshahnuralam">
                        <i class="fa fa-google-plus"></i>
                    </a>
                    <a class="btn btn-primary btn-sm" rel="publisher" href="https://secure.skype.com/portal/overview">
                        <i class="fa fa-skype"></i>
                    </a>
                    <a class="btn btn-warning btn-twitter btn-sm" href="https://twitter.com/webmaniac">
                        <i class="fa fa-twitter"></i>
                    </a>
                </div>
            </div>

        </div>

    </div>
</div>