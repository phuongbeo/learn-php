<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php" style="color: #000"><b>Adidas</b></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <!--            <ul class="nav navbar-nav">-->
        <!--                <li class="active"><a href="#">Link</a></li>-->
        <!--                <li><a href="#">Link</a></li>-->
        <!--            </ul>-->
        <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search" required>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
        <ul class="nav navbar-nav navbar-right">
            <?php session_start();
            if (empty($_SESSION['auth'])) { ?>
                <li><a href="dangnhap.php" class="button">Log in</a></li>
                <li><a href="dangky.php" class="button">Registration</a></li>
            <?php } else { ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php echo $_SESSION['auth']['username']; ?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="information.php">Account Information</a></li>
                        <li><a href="dangxuat.php">Log out</a></li>
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>