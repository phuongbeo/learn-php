<?php
$host = "127.0.0.1";
$dbname = "adidas";
$dbusername = "root";
$dbpassword = "";

try {
    $connect = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $dbusername, $dbpassword);

} catch (Exception $ex) {
    var_dump($ex->getMessage());
    die;
}

// câu query
$sql = "select * from slide ";
//$result = executeQuery($sql, true);
// nạp câu truy vấn vào kết nối
$stmt = $connect->prepare($sql);
// thực thi câu truy vấn với csdl
$stmt->execute();
//header('location: index.php');
?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php foreach ($stmt as $key => $u): ?>
            <div class="item <?php echo $key == 0 ? 'active' : '' ?>" >
                <img src="<?php echo $u['image'] ?>" title="<?php echo $u['title'] ?>" style="width:100%; height: 600px;">
            </div>
        <?php endforeach ?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <i style="margin-top: 300px; color: #bce8f1" class="fa fa-arrow-circle-left"></i>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <i style="margin-top: 300px; color: #bce8f1" class="fa fa-arrow-circle-right"></i>
    </a>
</div>