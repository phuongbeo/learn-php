<?php
require_once 'db_connection.php';
$sql = "select * from course ";
$stmt = executeQuery($sql, true);
?>
<!doctype html>
<html lang="en">
<?php include_once('layout/header.php')?>
<body>
<?php include_once ('layout/menu.php')?>
<div class="container">
<h2>Các khóa học online</h2>
    <div style="margin-top: 20px" class="row">
        <?php foreach ($stmt as $key => $u): ?>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="subjects.php?id=<?php echo $u['id']; ?>&name=<?php echo $u['name_course']; ?>">
                        <button style="float: right" class="btn btn-primary">Tham gia</button>
                    </a>
                    <h3 style="margin-top: 10px;color: #616161"><?php echo $u['name_course'] ?></h3>
                    <p style="color: #757575"><?php echo $u['title'] ?></p>
                </div>
            </div>
        </div>
        <?php endforeach ?>
    </div>
</div>
<?php include_once ('layout/footer.php')?>
</body>
</html>