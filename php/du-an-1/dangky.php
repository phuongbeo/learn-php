<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Đăng ký</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-8 col-sm-offset-4">
            <img src="assets/img/tải%20xuống.png" alt="" style="width: 300px;height: auto;margin-top: 50px;margin-bottom: -20px">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <form action="post-dangky.php" enctype="multipart/form-data" method="post" role="form" style="margin-top: 50px">
                <legend class="text-center">Đăng ký tài khoản</legend>
                <div class="form-group">
                    <label for="">Tên</label>
                    <input type="text" class="form-control" name="username" placeholder="Nhập tên" required>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Nhập email" required>
                </div>
                <div class="form-group">
                    <label for="">Mật Khẩu</label>
                    <input type="password" class="form-control" name="password" placeholder="Nhập mật khẩu" required>
                </div>
                <div class="form-group">
                    <label for="">Số Điện Thoai</label>
                    <input type="number" class="form-control" name="phone" placeholder="Nhập số điện thoại" required>
                </div>
                <div class="form-group">
                    <label for="">Thành Phố</label>
                    <input type="text" class="form-control" name="city" placeholder="Nhập thành phố" required>
                </div>
                <div class="form-group">
                    <label for="">Ngày Sinh</label>
                    <input type="date" class="form-control" name="date_of_birth" placeholder="Nhập ngày sinh" required>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-danger">Submit</button>
                    <a class="btn btn-primary" href="index.php">Back</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</body>
</html>