<!doctype html>
<html lang="en">
<?php include_once('layout/header.php')?>
<body>
<?php include_once('layout/menu.php')?>
<div class="container">
    <h4 class="text-primary">Thống Kê Các Môn Học</h4>
</div>
<!-- Styles -->
<style>
    #chartdiv {
        width: 100%;
        height: 500px;
    }

</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/material.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->
<script>
    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_material);
        am4core.useTheme(am4themes_animated);
// Themes end

        var chart = am4core.create("chartdiv", am4charts.PieChart);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.data = [
            {
                country: "Tiếng Anh ",
                value: 401
            },
            {
                country: "Toán",
                value: 300
            },
            {
                country: "Lý",
                value: 200
            },
            {
                country: "Hóa",
                value: 165
            },
            {
                country: "Ngữ Văn",
                value: 139
            },
            {
                country: "Địa Lý",
                value: 128
            }
        ];
        chart.radius = am4core.percent(70);
        chart.innerRadius = am4core.percent(40);
        chart.startAngle = 180;
        chart.endAngle = 360;

        var series = chart.series.push(new am4charts.PieSeries());
        series.dataFields.value = "value";
        series.dataFields.category = "country";

        series.slices.template.cornerRadius = 10;
        series.slices.template.innerCornerRadius = 7;
        series.slices.template.draggable = true;
        series.slices.template.inert = true;
        series.alignLabels = false;

        series.hiddenState.properties.startAngle = 90;
        series.hiddenState.properties.endAngle = 90;

        chart.legend = new am4charts.Legend();

    }); // end am4core.ready()
</script>

<!-- HTML -->
<div id="chartdiv"></div>
<?php include_once ('layout/footer.php')?>
</body>
</html>
