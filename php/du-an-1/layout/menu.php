<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"><img style="width: auto;height: 25px" src="assets/img/tải%20xuống.png" alt=""></a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Trang chủ</a></li>
                <li><a href="courses.php">Khóa học</a></li>
                <li><a href="profile.php">Cá Nhân</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php session_start();
                if ($_SESSION['auth'] == null) { ?>
                    <li><a href="dangnhap.php" class="button">Đăng nhập</a></li>
                    <li><a href="dangky.php" class="button">Đăng ký</a></li>
                <?php } else { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php echo $_SESSION['auth']['username'];
                            ?>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="doimatkhau.php">Đổi mật khẩu</a></li>
                            <li><a href="dangxuat.php">Đăng xuất</a></li>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
</div>