<?php
require_once 'db_connection.php';
$sql = "select * from users";
$stmt = executeQuery($sql, true);
?>
<!doctype html>
<html lang="en">
<?php include_once('layout/header.php')?>
<body>
<?php include_once ('layout/menu.php')?>
<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-primary">
                <div class="panel-heading">Thông tin tài khoản</div>
                <div class="panel-body">
                    <?php foreach ($stmt as $u): ?>
                    <div class="form-group">
                        <label for="">Tên</label>
                        <input type="text" class="form-control" value="<?php echo $u['username'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" class="form-control" value="<?php echo $u['email'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Số điện thoại</label>
                        <input type="number" class="form-control" value="<?php echo $u['phone'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Ngày sinh</label>
                        <input type="date" class="form-control" value="<?php echo $u['date_of_birth'] ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Thành phố</label>
                        <input type="text" class="form-control" value="<?php echo $u['city'] ?>">
                    </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once ('layout/footer.php')?>
</body>
</html>