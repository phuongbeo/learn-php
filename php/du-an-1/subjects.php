<?php
require_once 'db_connection.php';
$subjects_id = $_GET['id'];
$sql = "select * from subjects where id_course = $subjects_id";
$stmt = executeQuery($sql, true);
?>
<!doctype html>
<html lang="en">
<?php include_once('layout/header.php') ?>
<body>
<?php include_once('layout/menu.php') ?>
<div class="container">
    <div style="margin-top: 20px" class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <?php foreach ($stmt as $key => $u): ?>
                    <div class="panel-body">
                        <a href="#">
                            <button style="float: right" class="btn btn-primary">Tham gia</button>
                        </a>
                        <h4 style="margin-top: 10px;color: #616161;float: left">Bài <?php echo $u['id'] ?>
                            : <?php echo $u['title'] ?></h4>
                        <p class="text-center"
                           style="color: #757575;font-size: 16px;margin: 8px"><?php echo $u['content'] ?></p>
                    </div>
                <?php endforeach ?>
            </div>
        </div>

    </div>
</div>
<?php include_once('layout/footer.php') ?>
</body>
</html>
