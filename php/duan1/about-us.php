<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking hotel</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
    <link rel="stylesheet" href="public/css/about-us.css">
</head>
<body>
<?php require_once('layouts/menu.php') ?>

    <section class="container-fluid box-about-us">
        <div class="about-us">
            <div class="row about-bg-top">
                <label for="WELCOME TO HELIOS LEGEND HOTEL">WELCOME TO QUAT LAM HOTEL</label>
            </div>
            <div class="row about-content">
                <div class="about-content-img">
                    <img src="public/images/img1.jpg" alt="">
                </div>
                <div class="about-content-text">
                    <label for="Dear valued Guests,">Dear valued Guests,</label>
                    <p>Warmest greetings from QUAT LAM Hotel!</p>
                    <p>Centrally located at No. 2D Duong Thanh Street, Hoan Kiem District,
                         Hanoi, the new QUAT LAM Hotel is only minutes away from many
                          tourist attractions such as the Lake of Restored Sword 
                          (Hoan Kiem Lake), Ngoc Son Temple, Hanoi Opera house, the Water 
                          puppet theater and the peculiar Hanoi Old Quarter itself, Saving 
                          you tremendous of time exploring & discovering the charming city.
                           The most important thing is QUAT LAM is quite near by Little
                            - Indian Restaurant. That is the best choice for Muslims people.
                             They serve "Halal" food for Maylaysian, Indian, Indonesian....
                    </p>
                    <p>The new boutique Helios Legend Hotel processes 50 luxuriously-appointed
                         rooms, all with city view and large windows and are equipped with 
                         Air conditioner, free high speed internet access and wifi, IDD 
                         telephone, satellite TV, Mini Bar, luxurious bathrooms & high-end 
                         amenities. In addition all rooms have coffee and tea making 
                         facilities. Some room have balcony with city view
                    </p>
                    <p>Come And Enjoy your Happiness!</p>
                </div>
            </div>
        </div>

    </section>



<?php require_once('layouts/footer-bt.php') ?>
<?php require_once('layouts/footer.php') ?>
    
</body>
</html>