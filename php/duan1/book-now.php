<?php
require_once "db_connection.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $checkIn = $_POST['check_in'];
    $checkOut = $_POST['check_out'];
    $countPeople = $_POST['count_people'];
    if ($countPeople >= 3) {
        $sql = "select * from room_type where id = 6 or id = 8";
    } else {
        $sql = "select * from room_type where id = 7 or id = 9 or id = 10";
    }
    $stmt = executeQuery($sql, true);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking hotel</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
    <link rel="stylesheet" href="public/css/room-rate.css">
</head>
<body>
<?php require_once('layouts/menu.php') ?>
<div class="container">
    <div class="row mt-5">
        <div class="col-md-8">
            <?php if (isset($stmt)) { ?>
                <?php foreach ($stmt as $room_type): ?>
                    <div class="card mb-5">
                        <img src="<?php echo $room_type['image'] ?>" class="card-img-top" alt="">
                        <div class="card-body">
                            <h2 class="card-title"><?php echo $room_type['name'] ?></h2>
                            <div class="card-content">
                                <div class="card-content__left">
                                    <div class="cart-text"><?php echo $room_type['detail'] ?></div>
                                </div>
                                <div class="card-content__right">
                                    <p class="card-text text-danger font-weight-bold"
                                       style="font-size: 20px"><?php echo number_format($room_type['price']) ?>
                                        <sup>đ</sup>
                                    </p>
                                    <a href="checkout.php?id=<?php echo $room_type['id'] ?>&checkIn=<?php echo $checkIn ?>&checkOut=<?php echo $checkOut ?>&countPeople=<?php echo $countPeople ?>"
                                       class="btn btn-info pull-right">BOOK</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            <?php } ?>
        </div>
        <div class="col-md-4">

        </div>
    </div>
</div>
<?php require_once('layouts/footer-bt.php') ?>
<?php require_once('layouts/footer.php') ?>
</body>
</html>