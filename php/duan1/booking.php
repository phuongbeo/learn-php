<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking hotel</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/booking.css">
    <link rel="stylesheet" href="public/css/frontend.css">
</head>
<body>
<?php require_once('layouts/menu.php') ?>
<!-- booking -->
<section class="container-fluid booking-bg">
    <div class="container booking-page">
        <div class="row">

            <div class="col-md-7 col-12 booking-text">
                <div class="booking-text-item">
                    <h1>MAKE YOUR RESERVATION</h1>
                    <p>Warmest greetings from QUAT LAM Hotel!</p>
                </div>
            </div>
            <div class="col-md-5 booking-form">
                <div class="booking-form-item">
                    <form action="book-now.php" method="post">
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="form-lable" for="">Check In</label>
                                <input class="form-control" type="date" name="check_in"
                                       value="<?php echo date('Y-m-d'); ?>">
                            </div>
                            <div class="col-md-6">
                                <label class="form-lable" for="">Check Out</label>
                                <input class="form-control" type="date" name="check_out"
                                       value="<?php echo date('Y-m-d', strtotime('tomorrow')); ?>">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="form-lable" for="">Count People</label>
                                <select class="form-control" name="count_people">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-btn">
                            <div class="inner"></div>
                            <button type="submit" class="form-booking-submit">Check availability</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- endbooking -->
<?php require_once('layouts/footer-bt.php') ?>
<?php require_once('layouts/footer.php') ?>
</body>
</html>
