<?php
require_once "db_connection.php";

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $idRoom = $_GET['id'];
    $checkIn = $_GET['checkIn'];
    $checkOut = $_GET['checkOut'];
    $countPeople = $_GET['countPeople'];
    $sql = "select * from room_type where id = $idRoom";
    $data = executeQuery($sql, false);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking hotel</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
    <link rel="stylesheet" href="public/css/booking-pay.css">
</head>
<body>
<?php require_once('layouts/menu.php') ?>
<div class="container-fluid">
    <div class="container booking-pay-page">
        <div class="title text-center">
            <h1>Booking information</h1>
        </div>
        <div class="bk-pay">
            <div class="bk-pay-title">
                <h2>Your reservation - from <?php echo $checkIn ?> to <?php echo $checkOut ?>   </h2>
            </div>
            <div class="bk-recap-hotel">
                <table>
                    <thead>
                    <th>Quất Lâm Hotel</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Address</td>
                        <td>2D Duong Thanh Street - Hoan Kiem District - Hanoi - Vietnam</td>
                    </tr>
                    <tr>
                        <td>Reception is open</td>
                        <td>24/7</td>
                    </tr>
                    <tr>
                        <td>Check-in from</td>
                        <td><?php echo $checkIn ?></td>
                    </tr>
                    <tr>
                        <td>Check-out before</td>
                        <td><?php echo $checkOut ?></td>
                    </tr>
                    <tr>
                        <td>Count people</td>
                        <td><?php echo $countPeople ?> guests</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="row container">
                <div class="col-md-7 bk-recap-booking">
                    <div class="bk-recap-room">
                        <div class="row">
                            <div class="col-md-3">
                                <span><?php echo $data['name'] ?></span>
                            </div>
                            <div class="col-md-6">
                                <p><?php echo $data['detail'] ?></p>
                            </div>
                            <div class="col-md-3">
                                <p class="price">
                                    <?php echo number_format($data['price']) ?><sup>đ</sup>
                                </p>
                                <button class="btn btn-link collapseFeatures pull-right" type="button"
                                        data-toggle="collapse" data-target="#collapseFive" aria-expanded="false"
                                        aria-controls="collapseFive">
                                    <span>See image</span>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="collapse mt-5" id="collapseFive">
                                    <img src="<?php echo $data['image'] ?>" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="bk-recap-total">
                        <div class="row">
                            <div class="col-sm-3">
                                <span>Total</span>
                            </div>
                            <div class="col-sm-9">
                                <p class="price">
                                    <?php echo number_format($data['price']) ?><sup>đ</sup>
                                </p>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-5 bk-recap-form">
                    <h1>Guest information</h1>
                    <p>Could you please enter you
                        personal information in English</p>
                    <form action="post-booking.php" method="post">
                        <input type="number" name="id_room_type" value="<?php echo $data['id'] ?>" hidden>
                        <input type="date" name="check_in" value="<?php echo $checkIn ?>" hidden>
                        <input type="date" name="check_out" value="<?php echo $checkOut ?>" hidden>
                        <input type="number" name="count_people" value="<?php echo $countPeople ?>" hidden>
                        <input type="number" name="price" value="<?php echo $data['price'] ?>" hidden>
                        <div class="form-group">
                            <label for="">Fullname*</label>
                            <input type="text" class="form-control" name="fullname" placeholder="Fullname" required>
                        </div>
                        <div class="form-group">
                            <label for="">Email*</label>
                            <input type="email" class="form-control" name="email" placeholder="gmail@gmail.com" required>
                        </div>
                        <div class="form-group">
                            <label for="">Phone number*</label>
                            <input type="number" class="form-control" name="phone_number" placeholder="+84" required>
                        </div>
                        <div class="text-center">
                            <button class="submit-book" type="submit">BOOK NOW</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('layouts/footer-bt.php') ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script>
    $(function () {
        $('.collapseFeatures').click(function () {
            if ($(this).hasClass('expanded')) {
                $(this).removeClass('expanded');
                $(this).children('span').text('See image');
            } else {
                $(this).addClass('expanded');
                $(this).children('span').text('Hide image');
            }
        });
    })
</script>
</body>
</html>