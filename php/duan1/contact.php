<?php
require_once 'db_connection.php';

$sql = "select * from setting_booking where id = 1";
$result = executeQuery($sql, false);

if (isset($_GET['message'])) {
    $message = $_GET['message'];
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking hotel</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
    <link rel="stylesheet" href="public/css/contact-page.css">

</head>
<body>
<?php require_once('layouts/menu.php') ?>
<?php if (isset($message)) { ?>
    <p id="message" hidden><?php echo $message ?></p>
<?php } ?>
<div class="body">
    <div class="container-1">
        <div class="backg">
            <label for="">CONTACT US</label>
        </div>
        <div class="box-contact">
            <div class="contact">
                <div class="form">
                    <form action="contactMail.php" method="post">
                        <div class="form-text-t">
                            <strong><?php echo $result['name'] ?></strong>
                            <p>Add: <?php echo $result['address'] ?></p>
                            <p>Tel: + <?php echo $result['phone_hotel'] ?></p>
                            <p>Email: <?php echo $result['email'] ?></p>
                            <hr class="form-text-g">
                        </div>
                        <div class="form-group">
                            <label for="name">Full Name</label>
                            <input type="text" class="form-input" name="name" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Phone number</label>
                            <input type="number" class="form-input" name="phone_number" placeholder="Enter phone number">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-input" name="email" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="Message">Message</label>
                            <textarea type="text" name="content" class="form-text" cols="30" rows="7"></textarea>
                        </div>
                        <button type="submit" class="form-submit">SEND MESSAGE</button>
                    </form>
                </div>
                <div class="form-date">
                    <div class="form-date-in">
                        <p>MAKE A RESERVATION</p>
                        <div class="box-check">
                            <form action="book-now.php" method="post">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Check-in</p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="date" class="date" name="check_in"
                                               value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>Check out</p>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="date" class="date" name="check_out"
                                               value="<?php echo date('Y-m-d', strtotime('tomorrow')); ?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>People:</p>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="count_people">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="Availability">Search Availability</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<?php require_once('layouts/footer-bt.php') ?>
<?php require_once('layouts/footer.php') ?>
<script>
    const message = document.getElementById('message').innerText;
    message ? alert(message) : '';
</script>
</body>
</html>