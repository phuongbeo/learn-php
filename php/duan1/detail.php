<?php
require_once 'db_connection.php';
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $sqlDetail = "select * from room_type where id = $id";
    $room_typeDetail = executeQuery($sqlDetail, false);
}
$sql = "select * from room_type";
$stmt = executeQuery($sql, true);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking hotel</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
    <link rel="stylesheet" href="public/css/detail.css">
</head>
<body>
<?php require_once('layouts/menu.php') ?>
<!-- detail -->
<section class="detail-body">

    <div class="detail-room">
        <div class="detail-img-bg">
            <div class="detail-img-bg-text">
                <label for="PHÒNG PREMIER LAKE VIEW TWINS">PHÒNG PREMIER LAKE VIEW TWINS</label>
            </div>
            <div class="detail-img-slick">
                <img src="public/images/detail-img/bg-1.jpg" alt="">
                <img src="public/images/detail-img/bg-2.jpg" alt="">
                <img src="public/images/detail-img/bg-3.jpg" alt="">
            </div>
        </div>
        <div class="detail-information">
            <div class="information-body">
                <div class="row overview">
                    <div class=" col-sm-4 col-4 overview-people">
                        <div class="overview-icon-1"></div>
                        <b>02 NGƯỜI</b>
                    </div>
                    <div class="col-sm-4 col-4 overview-people">
                        <div class="overview-icon-2"></div>
                        <b>02 GIƯỜNG</b>
                    </div>
                    <div class=" col-sm-4 col-4 overview-people">
                        <div class="overview-icon-3"></div>
                        <b>46 M2</b>
                    </div>
                </div>
                <div class="row my-3 mx-1">
                    <div class="col-md-6 col-sm-6">
                        <img src="<?php echo $room_typeDetail['image'] ?>" alt="" class="img-fluid">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="text-info font-weight-bold"><?php echo $room_typeDetail['name'] ?></h2>
                                <p style="line-height: 35px"><?php echo $room_typeDetail['detail'] ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  -->
    <div class="container">
        <div class="box-list-hotel">
            <div class="row">
                <?php foreach ($stmt as $u): ?>
                    <div class="col-md-4 mb-5 list-hotel">
                        <div class="card box-card">
                            <img class="card-img-top img-fluid" src="<?php echo $u['image'] ?>" alt="Card image"
                                 style="height: 250px">
                            <div class="card-body">
                                <a href="detail.php?id=<?php echo $u['id'] ?>"
                                   class="card-link font-weight-bold text-warning"><?php echo $u['name'] ?></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</section>
<!-- detail -->
<?php require_once('layouts/footer-bt.php') ?>
<?php require_once('layouts/footer.php') ?>
<script>
    $('.detail-img-slick').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        pauseOnDotsHover: true,
        swipe: true,
        prevArrow: '<div class="slick-prev-detail" ></div>',
        nextArrow: '<div class="slick-next-detail" ></div>',
    });
</script>
</body>
</html>