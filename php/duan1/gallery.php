<?php
require_once 'db_connection.php';

$sql = "select * from room_type";
$result = executeQuery($sql, true);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking hotel</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
    <style>
        .hvrbox,
        .hvrbox * {
            box-sizing: border-box;
        }

        .hvrbox {
            position: relative;
            display: inline-block;
            overflow: hidden;
            max-width: 100%;
            height: auto;
        }

        .hvrbox img {
            max-width: 100%;
        }

        .hvrbox .hvrbox-layer_bottom {
            display: block;
        }

        .hvrbox .hvrbox-layer_top {
            opacity: 0;
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.6);
            color: #fff;
            padding: 15px;
            -moz-transition: all 0.4s ease-in-out 0s;
            -webkit-transition: all 0.4s ease-in-out 0s;
            -ms-transition: all 0.4s ease-in-out 0s;
            transition: all 0.4s ease-in-out 0s;
        }

        .hvrbox:hover .hvrbox-layer_top,
        .hvrbox.active .hvrbox-layer_top {
            opacity: 1;
        }

        .hvrbox .hvrbox-text {
            text-align: center;
            font-size: 20px;
            display: inline-block;
            position: absolute;
            top: 50%;
            left: 50%;
            -moz-transform: translate(-50%, -50%);
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }
    </style>
</head>
<body>
<?php require_once('layouts/menu.php') ?>
<div class="container">
    <h1 class="text-center mt-3 font-weight-bold text-info">Gallery image room</h1>
    <div class="row">
        <?php foreach ($result as $value): ?>
            <div class="col-md-4 my-5">
                <div class="hvrbox">
                    <img src="<?php echo $value['image'] ?>" alt="Mountains" class="hvrbox-layer_bottom" style="height: 250px">
                    <a href="detail.php?id=<?php echo $value['id'] ?>">
                        <div class="hvrbox-layer_top">
                            <div class="hvrbox-text"><?php echo $value['name'] ?></div>
                        </div>
                    </a>
                </div>
            </div>
        <?php endforeach ?>
    </div>
</div>
<?php require_once('layouts/footer-bt.php') ?>
<?php require_once('layouts/footer.php') ?>
</body>
</html>