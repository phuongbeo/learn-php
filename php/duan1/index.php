<?php
include_once "db_connection.php";

if (isset($_GET['booking'])) {
    $idBooking = $_GET['booking'];
    $sqlBooking = "select * from room_type where id = $idBooking";
    $booking = executeQuery($sqlBooking, false);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking hotel</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
</head>
<body>
<?php if (isset($idBooking)) { ?>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title text-success" id="exampleModalLabel">Congratulations on your successful booking ...!</h2>
                </div>
                <div class="modal-body">
                    <img src="<?php echo $booking['image'] ?>" alt="" class="img-fluid mb-4">
                    <b class="text-danger pull-right">Total: <?php echo number_format($booking['price']) ?> <sup>đ</sup></b>
                    <b class="text-info pull-left">Room type: <?php echo $booking['name'] ?></b>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php require_once('layouts/menu.php') ?>
<?php require_once('layouts/content.php') ?>
<?php require_once('layouts/footer-bt.php') ?>
<?php require_once('layouts/footer.php') ?>
    <script>
        $('.service-slick-slider').slick({
             slidesToShow: 3,
             slidesToScroll: 1,
             autoplay: true,
             autoplaySpeed: 2000,
             pauseOnDotsHover:true,
             swipe:true,
             prevArrow: '<div class="slick-prev" ></div>',
                nextArrow: '<div class="slick-next" ></div>',
             responsive: [
                {
                breakpoint: 768,
                settings: {
                slidesToShow: 1
                }
                } ]
            });
        $('#exampleModal').modal({
            show: true
        })
    </script>
</body>
</html>