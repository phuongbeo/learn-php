<?php
require_once 'db_connection.php';
$sql = "select * from room_type ORDER BY id LIMIT 3";
$stmt = executeQuery($sql, true);
?>
<section class="page-booking">
    <!-- slider -->
    <div class="bg-slider">
        <div class="booking">
            <div id="carouselExampleCaptions" class="carousel slide booking__slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="list-icon active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1" class="list-icon"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="2" class="list-icon"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="./public/images/img1.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="carousel-item">
                        <img src="./public/images/img2.jpg" class="img-fluid" alt="">
                    </div>
                    <div class="carousel-item">
                        <img src="./public/images/img3.jpg" class="img-fluid" alt="">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="booking__reservation">
                <div class="row">
                    <div class="col-md-3">
                        <h4 class="text-make">Make A Reservation</h4>
                    </div>
                    <div class="col-md-9">
                        <form action="book-now.php" method="post">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group-content">
                                        <label for="check-in">Check In</label>
                                        <input type="date" name="check_in" class="form-control"
                                               value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group-content">
                                        <label for="check-out">Check Out</label>
                                        <input type="date" class="form-control" name="check_out"
                                               value="<?php echo date('Y-m-d', strtotime('tomorrow')); ?>">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group-content">
                                        <label for="check-out">Count People</label>
                                        <select style="height: 35px;" class="custom-select custom-select-lg" name="count_people">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group-content">
                                        <button type="submit" class="Shearch-Availability">Search Availabilitly
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-color">

    </div>
    <div class="container">
        <div class="box-list-hotel">
            <div class="row">
                <?php foreach ($stmt as $u): ?>
                    <div class="col-md-4 mb-5 list-hotel">
                        <div class="card box-card">
                            <img class="card-img-top img-fluid" src="<?php echo $u['image'] ?>" alt="Card image"
                                 style="height: 250px">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo $u['name'] ?></h4>
                                <p class="card-text"><?php echo $u['detail'] ?></p>
                                <a href="detail.php?id=<?php echo $u['id'] ?>" class="btn btn-outline-info">Read
                                    more</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>

        </div>

    </div>
    <!--  -->
    <div class="service">
        <div class="service-img-bg">
            <div class="service-page">
                <p>Services</p>
                <section class="service-endow">
                    <div class="service-slick-slider">
                        <a href="#">
                            <div class="service-endow-item">
                                <div class="box-img-endow"><img src=" public/images/Uudai/Uudai-1.jpg" alt=""></div>
                                <label for="">Weekend Buffet Dinner – <br>VND 950,000 ++/người – Đi 4 trả 3</label>
                            </div>
                        </a>
                        <a href="#">
                            <div class="service-endow-item">
                                <div class="box-img-endow"><img src=" public/images/Uudai/Uudai-2.jpg" alt=""></div>
                                <label for="">Gói kỳ nghỉ “Amazing Stay” chỉ từ <br> VND 2,000,000++/đêm</label>
                            </div>
                        </a>
                        <a href="#">
                            <div class="service-endow-item">
                                <div class="box-img-endow"><img src=" public/images/Uudai/Uudai-3.jpg" alt=""></div>
                                <label for="">GOLDEN DAY 1st CELEBRATION</label>
                            </div>
                        </a>
                        <a href="#">
                            <div class="service-endow-item">
                                <div class="box-img-endow"><img src=" public/images/Uudai/Uudai-4.png" alt=""></div>
                                <label for="">ALL-DAY BREAKFAST – Chỉ <br>VND 200,000++/người</label>
                            </div>
                        </a>
                    </div>
                </section>
            </div>
        </div>
    </div>
</section>
