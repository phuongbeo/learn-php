<?php
require_once 'db_connection.php';

$sql ="select * from setting_booking where id = 1";
$result = executeQuery($sql, false);
?>
<footer>
    <div class="box-footer">
        <div class="container footer">
            <div class="row footer">
                <div class="col-md-3 col-sm-4 footer-text">
                    <img class="footer-logo" src="<?php echo $result['logo_url']?>" alt="" style="width: 155px;height: auto;">
                    <p> Copyright by <?php echo $result['name']?></p>
                </div>
                <div class="col-md-4 col-sm-5 footer-text">
                    <label for=""><?php echo $result['name']?></label>
                    <p>Address: <?php echo $result['address']?></p>
                    <p>Telephone: + <?php echo $result['phone_hotel']?></p>
                    <p>Email: <?php echo $result['email']?></p>
                </div>
                <div class="col-md-3  col-sm-4 footer-text">
                    <label for="">HOTEL INFORMATION</label>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#">Gallery</a></li>
                    <li><a href="#">About us</a></li>
                    <li><a href="contact.php">Contact us</a></li>
                    <li><a href="#">Room & Rate</a></li>
                    <li><a href="#">Booking</a></li>
                </div>
                <div class="col-md-2 col-sm-4 footer-text" >
                    <div class="ft-icon">
                        <img src="public/images/facebook.png" class="footer-icon-1" alt="">
                        <li><a href="#">Facebook</a></li>
                    </div>
                    <div class="ft-icon">
                        <img src="public/images/twitter.png" class="footer-icon-1" alt="">
                        <li><a href="#">Twitter</a></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>