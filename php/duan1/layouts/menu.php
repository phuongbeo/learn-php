<?php session_start(); ?>
<div class="bg-nav">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand text-info" href="index.php"><img src="./public/images/logo.png" style="width: 150px;height: auto;"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item setup-nav">
                            <a class="nav-link active" href="index.php">HOME</a><div class="hover-menu"></div>
                        </li>
                        <li class="nav-item setup-nav">
                            <a class="nav-link" href="about-us.php">ABOUT US</a><div class="hover-menu"></div>
                        </li>
                        <li class="nav-item setup-nav">
                            <a class="nav-link" href="gallery.php">GALLERY</a><div class="hover-menu"></div>
                        </li>
                        <li class="nav-item setup-nav">
                            <a class="nav-link" href="room-rate.php">ROOMS & RATE</a><div class="hover-menu"></div>
                        </li>
                        <li class="nav-item setup-nav">
                            <a class="nav-link" href="contact.php">CONTACT US</a><div class="hover-menu"></div>
                        </li>
                        <li class="nav-item setup-nav">
                            <a class="nav-link" href="booking.php">BOOKING</a><div class="hover-menu"></div>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php if (empty($_SESSION['auth'])) { ?>
                            <li class="nav-item setup-nav">
                                <a class="nav-link" href="login.php">Login / Register</a>
                            </li>
                        <?php } else { ?>
                            <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                m-dropdown-toggle="click">
                                <a href="#" class="m-nav__link m-dropdown__toggle">
                                <span class="m-topbar__userpic">
                                    <img src="https://ui-avatars.com/api/?name=<?php echo $_SESSION['auth']['name']; ?>"
                                         class="m--img-rounded m--marginless m--img-centered" width="35" height="35">
                                </span>
                                    <span class="m-topbar__username m--hide">
                                    Nick PhuongBeo
                                </span>
                                </a>
                                <div class="m-dropdown__wrapper">
                                <span
                                        class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__header m--align-center"
                                             style="background: url(./assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                                            <div class="m-card-user m-card-user--skin-dark">
                                                <div class="m-card-user__pic">
                                                    <img src="https://ui-avatars.com/api/?name=<?php echo $_SESSION['auth']['name']; ?>"
                                                         class="m--img-rounded m--marginless" alt=""/>
                                                </div>
                                                <div class="m-card-user__details">
                                            <span class="m-card-user__name m--font-weight-500">
                                                Hi, <?php echo $_SESSION['auth']['name']; ?>
                                            </span>
                                                    <a href="" class="m-card-user__email m--font-weight-300 m-link">
                                                        <?php echo $_SESSION['auth']['email']; ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav m-nav--skin-light">
                                                    <li class="m-nav__item nav-logout">
                                                        <a href="logout.php"
                                                           class="btn m-btn--pill btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">
                                                            Logout
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>
