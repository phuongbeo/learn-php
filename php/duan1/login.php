<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login page</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
</head>
<body class="bg-setup-form">
<?php require_once('layouts/menu.php') ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <?php
            if (isset($_GET['message'])) {
                echo "<div class='alert alert-success text-danger mt-3'>{$_GET['message']}</div>";
            }
            ?>
            <div class="card mt-5">
                <div class="card-body">
                    <h1 class="card-title font-weight-bold text-center text-info my-4">Login account</h1>
                    <form action="post-login.php" method="post" role="form" class="form-setup">
                        <div class="form-group">
                            <?php
                            if (isset($_GET['error'])) {
                                echo "<label class='text-danger'>{$_GET['error']}</label>";
                            }
                            ?>
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter email" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Enter password" required>
                        </div>
                        <hr class="my-4">
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger d-block w-100">Submit</button>
                            <p class="my-4 text-center">New account? <a href="register.php">Sign up here</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('layouts/footer.php') ?>
</body>
</html>