<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register page</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
</head>
<body class="bg-setup-form">
<?php require_once('layouts/menu.php') ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card my-5">
                <div class="card-body">
                    <h1 class="card-title font-weight-bold text-center text-warning my-4">Register account</h1>
                    <form action="post-register.php" method="post" role="form" class="form-setup">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter name" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter email" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Enter password" required>
                        </div>
                        <div class="form-group">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" name="address" placeholder="Enter address" required>
                        </div>
                        <div class="form-group">
                            <label for="phone_number">Phone number</label>
                            <input type="number" class="form-control" name="phone_number" placeholder="Enter phone number" required>
                        </div>
                        <hr class="my-4">
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger d-block w-100">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('layouts/footer.php') ?>
</body>
</html>