<?php
require_once "db_connection.php";
$sql = "select * from room_type";
$stmt = executeQuery($sql, true);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Booking hotel</title>
    <?php require_once('layouts/header.php') ?>
    <link rel="stylesheet" href="public/css/frontend.css">
    <link rel="stylesheet" href="public/css/room-rate.css">
</head>
<body>
<?php require_once('layouts/menu.php') ?>
 <section class="container-fluid room-rate">
        <div class="room-rate-page">
            <div class="room-rate-top">
                <h1 class="room-rate-title">Room</h1>
                <p>Với hệ thống phòng nghỉ sang trọng hiện đại, 
                    đầy đủ tiện nghi Dolce Golden Lake sẽ để lại 
                    ấn tượng sâu sắc cho bạn khi nghỉ tại đây.</p>
            </div>
            <?php foreach ($stmt as $value): ?>
            <div class="room-rate-item">
                    <div class="room-rate-inner">
                        <div class="img-room-item">
                            <img src="<?php echo $value['image'] ?>" alt="">
                        </div>
                        <div class="content-room-item">
                            <h2><?php echo $value['name'] ?></h2>
                            <span>Room</span>
                            <p><?php echo $value['detail'] ?></p>
                            <div class="content-room-readmore">
                                <a href="detail.php?id=<?php echo $value['id']?> ">Detail</a>
                            </div>
                            <a href="booking.php" class="btn-booking"><button>BOOK NOW</button></a>
                        </div>
                    </div>
            </div>
            <?php endforeach ?>
        </div>
 </section>
<?php require_once('layouts/footer-bt.php') ?>
<?php require_once('layouts/footer.php') ?>
</body>
</html>