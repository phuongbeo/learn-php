<?php
require_once '../db_connection.php';

$sql ="select * from room_type";
$result = executeQuery($sql, true);
$id = $_GET['id'];
$sqlRoom = "select * from rooms where id = $id";
$data = executeQuery($sqlRoom, false);
?>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>
        Admin | rooms
    </title>
    <meta name="description" content="Static table examples">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../public/css/backend.css">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <?php require_once('../layouts/header-admin.php') ?>
    <!--end::Base Styles -->
    <!--    <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />-->
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <?php include_once('../components/header.php') ?>
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
            <!-- BEGIN: Aside Menu -->
            <div
                id="m_ver_menu"
                class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                m-menu-vertical="1"
                m-menu-scrollable="0" m-menu-dropdown-timeout="500"
            >
                <?php
                include_once('menu-rooms.php');
                ?>
            </div>
            <!-- END: Aside Menu -->
        </div>
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <!-- END: Subheader -->
            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Edit room
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                <div class="table-responsive">
                                    <div class="col-md-12">
                                        <form action="update-room.php" method="post" role="form" class="form-setup">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="hidden" name="id" value="<?php echo $data['id'] ?>">
                                                    <div class="form-group">
                                                        <label for="room_type">Room type</label>
                                                        <select class="custom-select" id="inputGroupSelect01" name="room_type" required>
                                                            <?php foreach ($result as $u): ?>
                                                                <option value="<?php echo $u['id'] ?>" <?php echo $data['id_room_type'] == $u['id'] ? 'selected' : '' ?>><?php echo $u['name'] ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group mt-5">
                                                        <button type="submit" class="btn btn-danger mr-2">Update
                                                        </button>
                                                        <a href="index.php" class="btn btn-primary">Cancel</a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="name">Name</label>
                                                        <input type="text" class="form-control" name="name" value="<?php echo $data['name']?>"
                                                               placeholder="Enter name" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="price">Price</label>
                                                        <input type="number" class="form-control" name="price" value="<?php echo $data['price']?>"
                                                               placeholder="Enter price" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Body -->
    <!-- begin::Footer -->
    <?php include_once('../components/footer.php') ?>
    <!-- end::Footer -->
</div>
<?php require_once('../layouts/footer-admin.php') ?>
<!--end::Base Scripts -->
</body>
<!-- end::Body -->
</html>





