<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
    <li class="m-menu__item  m-menu__item--submenu"
        aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="../users/index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-avatar"></i>
            <span class="m-menu__link-text">
										Users
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="../rooms/index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-share"></i>
            <span class="m-menu__link-text">
										Rooms
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="../room_type/index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-interface-7"></i>
            <span class="m-menu__link-text">
										Room type
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="../comments/index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-tabs"></i>
            <span class="m-menu__link-text">
										Comments
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="../booking/index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-technology"></i>
            <span class="m-menu__link-text">
										Bookings
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu m-menu__item--open m-menu__item--expanded" aria-haspopup="true"
        m-menu-submenu-toggle="hover">
        <a href="index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-settings"></i>
            <span class="m-menu__link-text">
										Setting booking
									</span>
        </a>
    </li>
</ul>