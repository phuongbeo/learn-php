<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .title {
            color: #333;
        }
        .plan-header{
            height: 90px;
            background-color: white !important;
            border-bottom: 0 !important;
        }
        .plan-icon{
            font-size: 40px;
            color: greenyellow;
        }
        .plan-icon:hover{
            color: darkorange;
        }
        .plan-button{
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .plan-item {
            position: relative;
        }
        .plan-item.recommended:before {
            content: "Recommended";
            position: absolute;
            top: -40px;
            left: calc(50% - 50px);
        }
        .plan-item .card,
        .plan-item .plan-header {
            border-radius: 0.6rem !important;
        }
        @media (min-width: 1200px) {
            .plan-item {
                padding-left: 3px !important;
                padding-right: 3px !important;
                margin-bottom: 1.5rem;
            }
            .plan-item.recommended > div {
                margin-top: -15px;
            }
            .plan-item.recommended > div {
                padding-top: 15px;
            }
        }
        @media (max-width: 1199px) {
            .plan-item {
                margin-bottom: 2rem;
            }
            .plan-item.recommended:before {
                top: -25px;
            }
        }
    </style>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container my-5">
        <h3 class="text-center title">You are currently enjoying the <strong>free plan</strong></h3>
        <div class="row my-5">
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 plan-item">
                <div class="card border border-dark">
                    <div class="card-header plan-header">
                        <h5 class="card-title text-center">Free</h5>
                        <p class="card-subtitle mb-3 text-center text-muted">$0/month</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3 col-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9 col-9">
                                <b class="card-title">Classroom meeting</b>
                                <span class="d-block">1 active room</span>
                                <span class="d-block">4 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Lecture+ meeting</b>
                                <span class="d-block">1 active room</span>
                                <span class="d-block">8 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Lecture meeting</b>
                                <span class="d-block">1 active room</span>
                                <span class="d-block">12 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Livestream meeting</b>
                                <span class="d-block">1 active room</span>
                                <span class="d-block">20 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">whiteboard meeting</b>
                                <span class="d-block">1 active room</span>
                                <span class="d-block">20 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                    </div>
                    <div class="plan-button text-center pb-4">
                        <button type="button" class="btn btn-outline-secondary w-75">Your plan</button>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 plan-item">
                <div class="card border border-dark">
                    <div class="card-header plan-header">
                        <h5 class="card-title text-center">Basic</h5>
                        <p class="card-subtitle mb-3 text-center text-muted">$5/month <br> $50/year</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Classroom meeting</b>
                                <span class="d-block">2 active rooms</span>
                                <span class="d-block">4 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Lecture+ meeting</b>
                                <span class="d-block">2 active rooms</span>
                                <span class="d-block">8 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Lecture meeting</b>
                                <span class="d-block">2 active rooms</span>
                                <span class="d-block">12 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Livestream meeting</b>
                                <span class="d-block">2 active rooms</span>
                                <span class="d-block">20 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Whiteboard meeting</b>
                                <span class="d-block">2 active rooms</span>
                                <span class="d-block">20 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                    </div>
                    <div class="plan-button text-center pb-4">
                        <button type="button" class="btn btn-primary d-block w-75">Upgrade</button>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 plan-item recommended">
                <div class="card border border-dark">
                    <div class="card-header plan-header">
                        <h5 class="card-title text-center">Premium</h5>
                        <p class="card-subtitle mb-3 text-center text-muted">$10/month <br> $90/year</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Classroom meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">6 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Lecture+ meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">12 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Lecture meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">12 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Livestream meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">20 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Whiteboard meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">20 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                    </div>
                    <div class="plan-button text-center pb-4">
                        <button type="button" class="btn btn-primary d-block w-75">1 month free trial</button>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 plan-item">
                <div class="card border border-dark">
                    <div class="card-header plan-header">
                        <h5 class="card-title text-center">Pro</h5>
                        <p class="card-subtitle mb-3 text-center text-muted">$20/month <br> $160/year</p>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Classroom meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">10 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Lecture+ meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">12 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Lecture meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">20 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Livestream meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">50 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                        <div class="row">
                            <div class="col-sm-3 col-3">
                                <i class="fas fa-check-circle plan-icon my-3"></i>
                            </div>
                            <div class="col-sm-9 col-9">
                                <b class="card-title">Whiteboard meeting</b>
                                <span class="d-block">Unlimited room</span>
                                <span class="d-block">100 guests</span>
                            </div>
                        </div>
                        <div class="border-bottom mx-2 my-1"></div>
                    </div>
                    <div class="plan-button text-center pb-4">
                        <button type="button" class="btn btn-primary d-block w-75">Upgrade</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>