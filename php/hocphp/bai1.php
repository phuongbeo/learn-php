<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>hằng, biến, phép toán, toán tử, kiểu dữ liệu trong PHP</title>
</head>
<body>
	<?php 
	define(PI, 3.14);
	echo("giá trị của PI là: " . PI);
	$a = 5;
	$b = 10;
	$tong = $a + $b;
	$hieu = $a - $b;
	$nhan = $a * $b;
	$chia = $a / $b;
	$du = 5 % 4;
	echo("<br> $a + $b = $tong");
	echo("<br> $a - $b = $hieu");
	echo("<br> $a * $b = $nhan");
	echo("<br> $a / $b = $chia");
	echo("<br> 5 % 4 = $du");
	?>
</body>
</html>