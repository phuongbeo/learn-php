<?php 
$host = "127.0.0.1";
$dbname = "assignment_php1"; // tên database - assignment_php1
$dbhotel_name = "root";
$dbimage = "";
try{
	$connect = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $dbhotel_name, $dbimage);	
	
}catch(Exception $ex){
	var_dump($ex->getMessage());
	die;
}
// câu query
$sql = "select * from hotels";
// nạp câu truy vấn vào kết nối
$stmt = $connect->prepare($sql);
// thực thi câu truy vấn với csdl
$stmt->execute();
// thu thập kết quả trả về
$result = $stmt->fetchAll();
?>
<table>
	<thead>
		<tr>
			<th>Id</th>
			<th>Hotel name</th>
			<th>image</th>
			<th>
				<a href="add-hotel.php" title="">Add new</a>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($result as $u): ?>
			<tr>
				<td><?php echo $u['id'] ?></td>
				<td><?php echo $u['hotel_name'] ?></td>
				<td>
					<img src="<?php echo $u['image'] ?>" width="100">
				</td>
				<td>
					<a href="edit-hotel.php?id=<?php echo $u['id']?>" title="">Edit</a>
					<a href="remove-hotel.php?id=<?php echo $u['id']?>" title="">Remove</a>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
