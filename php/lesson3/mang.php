<?php 
// khai báo mảng tuần tự index
$a = array("phương", "lan", "ngọc", "nam", "hai");
//lấy ra một giá trị của mảng
echo("<br> Họ và tên: " . $a[0]); 
//lấy ra tất cả giá trị của mảng
foreach ($a as $name) {
	echo("<br> tất cả dữ liệu mảng: " . $name);
}
// khai báp mảng không tuần tự dạng key
$mang = array(
	"họ tên" => "Nguyễn Thanh phương",
	"giới tính" => "nam",
	"cmnd" => "036099007472",
	"quê quán" => "Nam Định",
	"số điện thoại" => "0974667645",
	"nơi cư trú" => "Hà Nội",
	"tình trạng hôn nhân" => "độc thân"
);
//lấy ra một giá trị trong mảng
echo("<br> họ tên: " . $mang["họ tên"]);
//lấy ra tất cả dữ liệu của mảng
foreach($mang as $key => $value){
	echo("<br>" . $key . ": " . $value);
}
?>
