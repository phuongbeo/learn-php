<?php 
/*
 * Tìm hiểu các hàm dưới đây và lấy ví dụ
 * crypt() //bam chuoi binh thuong thanh 1 chuoi ma hoa
 * explode()  // convert string  to array
 * htmlentities() //giu nguyen dinh dang cua the html khi viet trong php
 * implode() //bien doi mang ve chuoi
 * join() // giong het implode
 * lcfirst() // lower case first
 * trim() // xoa ki tu o dau va cuoi trung voi dinh dang dua vao
 * ltrim() //xoa trai
 * rtrim() //xoa phai
 * str_replace() // thay the ki tu trong chuoi
 * str_repeat() // lap lai ki tu trong chuoi
 * strlen() // dem so luong ki tu trong 1 chuoi
 * strpos() //tim vi tri dau tien cua ki tu dua vao
 * substr() // xoa 1 phan trong chuoi dựa vao vi tri dau tien va ket thuc
 * ucwords() //viet hoa tat ca ki tu dau tien cua cac tu trong chuoi
 * password_hash() // giong crypt
 * password_verify() // kiem tra xem chuoi nhap vao co giong chuoi da duoc bam hay khong
 */
#trim() - loại bỏ khoảng trắng ở 2 đầu của string
$str = "  something ";
var_dump(trim($str));
 ?>