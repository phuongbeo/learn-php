-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2020 at 01:33 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lesson6`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`) VALUES
(1, 'nhân sự'),
(2, 'bảo vệ'),
(3, 'lao công'),
(4, 'It support');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'https://upload.wikimedia.org/wikipedia/commons/1/1e/Default-avatar.jpg',
  `price` int(11) NOT NULL,
  `detail` text NOT NULL,
  `brand` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `image`, `price`, `detail`, `brand`) VALUES
(1, 'Iphone 6', 'https://cdn.tgdd.vn/Products/Images/42/92962/iphone-6-32gb-gold-hh-600x600-600x600-600x600.jpg', 6500, 'iPhone 6 là một trong những smartphone được yêu thích nhất của Apple. Lắng nghe nhu cầu về thiết kế, khả năng lưu trữ và giá cả', 'Apple'),
(2, 'Iphone X', 'https://cdn.tgdd.vn/Products/Images/42/114115/iphone-x-64gb-1-400x460-1-400x460.png', 22000, 'iPhone X là cụm từ được rất nhiều người mong chờ muốn biết và tìm kiếm trên Google bởi đây là chiếc điện thoại mà Apple kỉ niệm 10 năm iPhone đầu tiên được bán ra.', 'Apple'),
(3, 'Xiaomi redmi note 7', 'https://cdn.tgdd.vn/Products/Images/42/167535/xiaomi-redmi-note-7-400x460.png', 5000, 'Xiaomi Redmi Note 7 là chiếc smartphone giá rẻ mới được giới thiệu vào đầu năm 2019 với nhiều trang bị đáng giá như thiết kế notch giọt nước hay camera lên tới 48 MP.', 'Xiaomi '),
(4, 'Huawei Mate 20 Pro', 'https://cdn.tgdd.vn/Products/Images/42/188963/huawei-mate-20-pro-purple-400x460.png', 16000, 'Thế hệ siêu phẩm mới của Huawei chính thức lộ diện với cái tên Huawei Mate 20 Pro, chiếc điện thoại thu hút trong thiết kế, mạnh mẽ trong hiệu năng cùng một hệ thống camera ấn tượng.', 'Huawei '),
(5, 'Galaxy Note 9', 'https://images-na.ssl-images-amazon.com/images/I/61Lpcw%2Bnv1L._SX569_.jpg', 22000, 'Thế hệ smartphone quyền năng mang đậm dấu ấn của bạn.', 'Samsung');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `avatar` varchar(500) NOT NULL DEFAULT '',
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `avatar`, `department_id`) VALUES
(1, 'admin', '123456', 'thanhphuongnd99@gmail.com', 'https://scontent.fhan5-7.fna.fbcdn.net/v/t1.0-1/p160x160/64854484_2347688272170658_2698769745850662912_n.jpg?_nc_cat=103&_nc_oc=AQkNeiRVpx1_4DLcbl3aOyclisqxIpwfH9Mid-L4HU_OWQ2ZVQJqlc8N7WgQtMPDD6R6sM-AFvsmm3XobEVNjvkp&_nc_ht=scontent.fhan5-7.fna&oh=fb0557bc0fc6cfee3b44b95899ab1e81&oe=5DA66848', 2),
(2, 'nguyen thanh phuong', '654321', 'phuongntph08862@fpt.edu.vn', 'https://scontent.fhan5-4.fna.fbcdn.net/v/t1.0-9/61994178_2337766143162871_2658840302483144704_n.jpg?_nc_cat=104&_nc_oc=AQnusM3w_wZbzE6gqs0XJmhqIoJxgwLxON8S-AOZcN1XZcGzGOxeM1Vp3mflWr-D5iNgNxax29sp9QLyZMSAuw2x&_nc_ht=scontent.fhan5-4.fna&oh=37a8a6fe40ab0b98cec1da9b4be2af64&oe=5DE6CA9B', 4),
(9, 'nam', '123456', 'nam@gmail.co', 'uploads/5d4e65dda1b88-15578590_1816615661944591_1957594659212250041_n.jpg', 2),
(11, 'phuong', '123456', 'phuong@gmail.com', 'uploads/5d50df35034d0-29791344_2047713615501460_2336482949421072384_n.jpg', 1),
(12, 'thor', '123456', 'thor@gmail.com', '', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
