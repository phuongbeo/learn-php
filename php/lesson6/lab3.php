 <?php 
$host = "127.0.0.1";
$dbname = "lesson6"; // ten database bang tem lesson6
$dbproduct_name = "root";
$dbimage = "";
try{
	$connect = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8" , $dbproduct_name, $dbimage);
	// echo 'done';
}catch(Exception $ex){
	var_dump($ex->getMessage());
}
// cau query
$sql = "select * from products";
// nap cau truy van vao ket noi
$stmt = $connect->prepare($sql);
// thuc thi cau truy van voi csdl
$stmt->execute();
//thu thap ket qua tra ve
$result = $stmt->fetchAll();
// echo "<pre>";
// var_dump($result);
?>
<table>
	<thead>
		<tr>
			<th>Tên sản phẩm</th>
			<th>Ảnh</th>
			<th>Giá sản phẩm</th>
			<th>Chi tiết</th>
			<th>Thương hiệu</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($result as $u): ?>
			<tr>
				<td><?php echo $u["product_name"]?></td>			
				<td>
					<img src="<?php echo $u["image"]?>" width="100">
				</td>	
				<td><?php echo $u["price"]?></td>	
				<td width="400"><?php echo $u["detail"]?></td>	
				<td><?php echo $u["brand"]?></td>	
			</tr>
		<?php endforeach ?>
	</tbody>
</table>