<?php

require_once 'db_connection.php';
//$sql = "select * from rooms";
$sqlUser = "select * from users";
$userCount = executeQuery($sqlUser, true);
$sqlList = "select * from lists";
$listCount = executeQuery($sqlList, true);
$sqlPost = "select * from posts";
$postCount = executeQuery($sqlPost, true);
$sqlComment = "select * from comments";
$commentCount = executeQuery($sqlComment, true);
?>
<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>
        Admin
    </title>
    <meta name="description" content="Static table examples">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <?php require_once('layouts/header.php') ?>
    <!--end::Base Styles -->
    <!--    <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />-->
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <?php include_once('components/header-admin.php') ?>
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
            <!-- BEGIN: Aside Menu -->
            <div
                    id="m_ver_menu"
                    class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                    m-menu-vertical="1"
                    m-menu-scrollable="0" m-menu-dropdown-timeout="500"
            >
                <?php
                include_once('components/menu.php');
                ?>
            </div>
            <!-- END: Aside Menu -->
        </div>
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <!-- END: Subheader -->
            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Dashboard
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row justify-content-between">
                            <div class="col-md-5">
                                <div class="m-widget4">
                                    <div class="m-widget4__item">
                                        <div class="m-widget4__img m-widget4__img--logo">
                                            <h1 class="text-warning"><?php echo count($userCount) ?></h1>
                                        </div>
                                        <div class="m-widget4__info">
                                            <a class="text-secondary"
                                               href="users/index.php">
                                                <i class="fa fa-user"></i>
                                                Total accounts
                                            </a>
                                            <br>
                                            <span class="m-widget4__sub">accounts created by admin and customer</span>
                                        </div>
                                        <a href="users/index.php"
                                           title="View"
                                           class="btn btn-outline-info m-btn m-btn--icon btn-sm    m-btn--icon-only m-btn--pill m-btn--air">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="m-widget4">
                                    <div class="m-widget4__item">
                                        <div class="m-widget4__img m-widget4__img--logo">
                                            <h1 class="text-warning"><?php echo count($listCount) ?></h1>
                                        </div>
                                        <div class="m-widget4__info">
                                            <a class="text-secondary"
                                               href="lists/index.php">
                                                <i class="fa fa-list-alt"></i>
                                                Total Lists
                                            </a>
                                            <br>
                                            <span class="m-widget4__sub">Lists created by admin</span>
                                        </div>
                                        <a href="lists/index.php"
                                           title="View"
                                           class="btn btn-outline-info m-btn m-btn--icon btn-sm    m-btn--icon-only m-btn--pill m-btn--air">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="m-widget4">
                                    <div class="m-widget4__item">
                                        <div class="m-widget4__img m-widget4__img--logo">
                                            <h1 class="text-warning"><?php echo count($postCount) ?></h1>
                                        </div>
                                        <div class="m-widget4__info">
                                            <a class="text-secondary"
                                               href="posts/index.php">
                                                <i class="fa fa-sliders"></i>
                                                Total posts
                                            </a>
                                            <br>
                                            <span class="m-widget4__sub">posts created by admin</span>
                                        </div>
                                        <a href="posts/index.php"
                                           title="View"
                                           class="btn btn-outline-info m-btn m-btn--icon btn-sm    m-btn--icon-only m-btn--pill m-btn--air">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="m-widget4">
                                    <div class="m-widget4__item">
                                        <div class="m-widget4__img m-widget4__img--logo">
                                            <h1 class="text-warning"><?php echo count($commentCount) ?></h1>
                                        </div>
                                        <div class="m-widget4__info">
                                            <a class="text-secondary"
                                               href="comments/index.php">
                                                <i class="fa fa-commenting" aria-hidden="true"></i>
                                                Total comments
                                            </a>
                                            <br>
                                            <span class="m-widget4__sub">comments created by customer</span>
                                        </div>
                                        <a href="comments/index.php"
                                           title="View"
                                           class="btn btn-outline-info m-btn m-btn--icon btn-sm    m-btn--icon-only m-btn--pill m-btn--air">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Body -->
    <!-- begin::Footer -->
    <?php include_once('components/footer.php') ?>
    <!-- end::Footer -->
</div>
<?php require_once('layouts/footer-admin.php') ?>
<!--end::Base Scripts -->
</body>
<!-- end::Body -->
</html>
