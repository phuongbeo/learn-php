<?php

require_once 'db_connection.php';
$id = $_GET['id'];
$sql = "select p.*, l.name as list from posts p join lists l on p.id_list = l.id where p.id_list = $id order by id desc";
$stmt = executeQuery($sql, true);

$sqlLst = "select * from lists";
$dataList = executeQuery($sqlLst, true);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VNCoder | Category</title>
    <link rel="stylesheet" href="public/css/frontend.css">
    <?php require_once('layouts/header.php') ?>
</head>
<body>
<?php require_once('layouts/header-menu.php') ?>
<?php require_once('layouts/menu.php') ?>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h1 class="title-des my-4">KIẾN THỨC LẬP TRÌNH</h1>
            <div class="row">
                <?php foreach ($stmt as $u): ?>
                    <div class="col-md-6 mb-5">
                        <div class="box-post">
                            <img src="<?php echo $u['image'] ?>" alt="" style="width: 30%;height: auto;">
                            <div class="box-post__content">
                                <a href="detail.php?id=<?php echo $u['id']?>&list=<?php echo $u['list']?>"><h4 class="title"><?php echo $u['title'] ?></h4></a>
                                <hr class="hr-space">
                                <h6 class="list"><?php echo $u['list'] ?></h6>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="col-md-4">
            <ul class="list-group mt-4">
                <li class="list-group-item active">Category posts</li>
                <?php foreach ($dataList as $list): ?>
                    <li class="list-group-item"><a href="category.php?id=<?php echo $list['id'] ?>" style="color: #000"><?php echo $list['name'] ?></a></li>
                <?php endforeach ?>
            </ul>
            <img src="./public/images/learn-css.jpg" alt="" class="mb-5 img-fluid w-100">
            <img src="./public/images/learn-js.jpg" alt="" class="mb-5 img-fluid w-100">
            <img src="./public/images/learn-php.jpg" alt="" class="img-fluid w-100">
        </div>
    </div>
</div>
<?php require_once('layouts/footer.php') ?>
<?php require_once('layouts/script.php') ?>
</body>
</html>
