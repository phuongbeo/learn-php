<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
    <li class="m-menu__item  m-menu__item--submenu"
        aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-avatar"></i>
            <span class="m-menu__link-text">
										Users
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="../lists/index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-users"></i>
            <span class="m-menu__link-text">
										Lists
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="../posts/index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-list"></i>
            <span class="m-menu__link-text">
										Posts
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu m-menu__item--open m-menu__item--expanded" aria-haspopup="true"
        m-menu-submenu-toggle="hover">
        <a href="../comments/index.php" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-interface-7"></i>
            <span class="m-menu__link-text">
										Comments
									</span>
        </a>
    </li>
</ul>