<?php

require_once 'db_connection.php';
$id = $_GET['id'];
$list = $_GET['list'];

$sql = "select * from posts where id = $id";
$stmt = executeQuery($sql, false);

if (isset($_GET['idCmt'])) {
    $idCmt = $_GET['idCmt'];
    $sqlEditCmt = "select * from comments where id = $idCmt";
    $editCmt = executeQuery($sqlEditCmt, false);
}

$sqlComment = "select * from comments where id_post = $id ORDER BY date DESC";
$comments = executeQuery($sqlComment, true);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detail</title>
    <link rel="stylesheet" href="public/css/frontend.css">
    <link rel="stylesheet" href="public/css/detail.css">
    <?php require_once('layouts/header.php') ?>
</head>
<body>
<?php require_once('layouts/header-menu.php') ?>
<?php require_once('layouts/menu.php') ?>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="detail-page">
                <p class="detail-page__list my-4"><?php echo $list ?></p>
                <div class="row">
                    <div class="col-md-12 mb-5">
                        <h1 class="detail-page__title"><?php echo $stmt['title'] ?></h1>
                        <img src="<?php echo $stmt['image'] ?>" class="detail-page__img" alt=""
                             style="width: 30%;height: auto;">
                        <h6 class="detail-page__content"><?php echo $stmt['content'] ?></h6>
                    </div>
                </div>
            </div>
            <form action="comments/post-comment.php" method="post" role="form" class="form-setup">
                <div class="row">
                    <div class="col-md-5">
                        <input type="text" name="id_post" value="<?php echo $stmt['id'] ?>" hidden>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Enter email" required>
                        </div>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter name" required>
                        </div>
                        <hr class="my-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger d-block w-100">Post comment</button>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label for="content">Comment</label>
                            <textarea name="content" cols="30" rows="10" class="form-control" required></textarea>
                        </div>
                    </div>
                </div>
            </form>
            <div class="comments-list mt-4">
                <h2 class="">Current comments</h2>
                <hr>
                <?php foreach ($comments as $key => $u): ?>
                    <div class="media mb-3">
                        <a class="media-left" href="#">
                            <img src="https://ui-avatars.com/api/?name=<?php echo $u['name'] ?>"
                                 style="border-radius: 50% ">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading ml-3"><?php echo $u['name'] ?></h4>
                                <p class="ml-3"><?php echo $u['content'] ?></p>
                        </div>
                        <div class="pull-right">
                            <p><?php echo $u['date'] ?></p>
                            <a href="detail.php?id=<?php echo $id ?>&list=<?php echo $list ?>&idCmt=<?php echo $u['id'] ?>"
                               title="Edit"
                               class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air">
                                <i class="fa flaticon-edit"></i>
                            </a>
                            <a href="comments/remove.php?id=<?php echo $u['id'] ?>"
                               title="Delete"
                               class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"
                               onclick="return confirm('Want to delete?');">
                                <i class="fa flaticon-delete-2"></i>
                            </a>
                        </div>
                    </div>
                <?php endforeach ?>
                <?php if (isset($idCmt)) { ?>
                    <form action="comments/update-comment.php" method="post" role="form"
                          class="form-setup">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                <textarea name="content" cols="30" rows="5" class="form-control"
                                          required><?php echo $editCmt['content'] ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="id_update_cmt"
                                       value="<?php echo $editCmt['id'] ?>" hidden>
                                <input type="text" name="id_post"
                                       value="<?php echo $id ?>" hidden>
                                <input type="text" name="list"
                                       value="<?php echo $list ?>" hidden>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-danger d-block w-100">
                                        Send
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-4">
            <img src="./public/images/learn-css.jpg" alt="" class="mb-5 img-fluid">
            <img src="./public/images/learn-js.jpg" alt="" class="mb-5 img-fluid">
            <img src="./public/images/learn-php.jpg" alt="" class="img-fluid">
        </div>
    </div>
</div>
<?php require_once('layouts/footer.php') ?>
<?php require_once('layouts/script.php') ?>
</body>
</html>
