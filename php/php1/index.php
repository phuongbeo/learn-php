<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>VNCoder</title>
    <link rel="stylesheet" href="public/css/frontend.css">
    <?php require_once('layouts/header.php') ?>
</head>
<body>
<?php require_once('layouts/header-menu.php') ?>
<?php require_once('layouts/menu.php') ?>
<?php require_once('layouts/content.php') ?>
<?php require_once('layouts/footer.php') ?>
<?php require_once('layouts/script.php') ?>
</body>
</html>