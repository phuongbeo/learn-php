<?php

require_once 'db_connection.php';

$sql = "select p.*, l.name as list from posts p join lists l on p.id_list = l.id order by id desc limit 10";
$stmt = executeQuery($sql, true);

?>
<div class="container">
    <div class="row">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <?php foreach ($dataList as $key => $list): ?>
                <div class="carousel-item <?php echo $key == 0 ? 'active' : '' ?>">
                    <img src="./<?php echo $list['image'] ?>" alt="" title="<?php echo $list['name'] ?>" height="600" width="100%">
                    <div class="carousel-caption d-none d-md-block">
                        <a href="category.php?id=<?php echo $list['id'] ?>" style="text-decoration: none"><h2 class="font-weight-bold text-warning"><?php echo $list['name'] ?></h2></a>
                        <p style="font-size: 20px">Click to view programming news</p>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <h1 class="title-des my-4">KIẾN THỨC LẬP TRÌNH (New)</h1>
            <div class="row">
                <?php foreach ($stmt as $u): ?>
                    <div class="col-md-6 mb-5">
                        <div class="box-post">
                            <img src="<?php echo $u['image'] ?>" alt="" style="width: 30%;height: auto;">
                            <div class="box-post__content">
                                <a href="detail.php?id=<?php echo $u['id'] ?>&list=<?php echo $u['list'] ?>"><h4
                                            class="title"><?php echo $u['title'] ?></h4></a>
                                <hr class="hr-space">
                                <a class="text-dark" href="category.php?id=<?php echo $u['id_list'] ?>"><h6
                                            class="list"><?php echo $u['list'] ?></h6></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
        <div class="col-md-4">
            <img src="./public/images/learn-css.jpg" alt="" class="mb-5 img-fluid w-100">
            <img src="./public/images/learn-js.jpg" alt="" class="mb-5 img-fluid w-100">
            <img src="./public/images/learn-php.jpg" alt="" class="img-fluid w-100">
        </div>
    </div>
</div>
