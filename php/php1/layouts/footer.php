<div class="bg-footer">
    <div class="container">
        <footer>
            <div class="row">
                <div class="col-md-4">
                    <img src="./public/images/logo_php1.png" alt="" class="img-fluid" style="padding-top: 25px;">
                </div>
                <div class="col-md-4">
                    <h6 class="text-uppercase font-weight-bold text-center">Pages</h6>
                    <ul class="list-unstyled text-center">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a href="#">Login</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li>
                        <li>
                            <a href="#">Register</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h6 class="text-uppercase font-weight-bold">Contact</h6>
                    <ul class="list-unstyled">
                        <li>
                            <p>Address: Dương Nội - Hà Đông - Hà Nội</p>
                        </li>
                        <li>
                            <p>Mail: thanhphuongnd99@gmail.com</p>
                        </li>
                        <li>
                            <p>Phone: 0974667645</p>
                        </li>
                        <li>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
</div>
<div class="footer-copyright text-center py-3 bg-copyright">© 2020 Copyright
    <a href="#"> VNcode</a>
</div>
