<?php
require_once '../db_connection.php';

$sql ="select * from lists";
$result = executeQuery($sql, true);
?>
<html lang="en">
<!-- begin::Head -->
<head>
    <meta charset="utf-8"/>
    <title>
        Admin | lists
    </title>
    <meta name="description" content="Static table examples">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../public/css/backend.css">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <?php require_once('../layouts/header-admin.php') ?>
    <!--end::Base Styles -->
    <!--    <link rel="shortcut icon" href="assets/demo/default/media/img/logo/favicon.ico" />-->
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <?php include_once('../components/header.php') ?>
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
            <!-- BEGIN: Aside Menu -->
            <div
                id="m_ver_menu"
                class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                m-menu-vertical="1"
                m-menu-scrollable="0" m-menu-dropdown-timeout="500"
            >
                <?php
                include_once('menu-lists.php');
                ?>
            </div>
            <!-- END: Aside Menu -->
        </div>
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <!-- END: Subheader -->
            <div class="m-content">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Lists Management
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="create-list.php" class="btn btn-info"><i
                                            class="fa flaticon-plus"></i> Create list</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                <div class="table-responsive">
                                    <?php
                                    if (isset($_GET['message'])) {
                                        $mess = $_GET['message'];
                                        echo "<div class='alert alert-success text-danger mt-3'>{$_GET['message']}</div>";
                                    }
                                    ?>
                                    <table class="table table-striped m-table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>
                                                Edit
                                            </th>
                                            <th>
                                                Delete
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($result as $key => $u): ?>
                                            <tr>
                                                <td><?php echo ++$key ?></td>
                                                <td><?php echo $u['name'] ?></td>
                                                <td>
                                                    <img src="../<?php echo $u['image'] ?>" alt="" width="200">
                                                </td>
                                                <td>
                                                    <a href="edit-list.php?id=<?php echo $u['id'] ?>"
                                                       title="Edit"
                                                       class="btn btn-outline-warning m-btn m-btn--icon btn-sm    m-btn--icon-only m-btn--pill m-btn--air">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="remove.php?id=<?php echo $u['id'] ?>"
                                                       title="Delete"
                                                       class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air " onclick="return confirm('Want to delete?');">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Body -->
    <!-- begin::Footer -->
    <?php include_once('../components/footer.php') ?>
    <!-- end::Footer -->
</div>
<?php require_once('../layouts/footer-admin-child.php') ?>
<!--end::Base Scripts -->
</body>
<!-- end::Body -->
</html>





