<?php
require_once app_path . '/Model/AdminModel.php';
class AdminController extends ControllerBase
{
    public function Index()
    {
        $objUserModel = new AdminModel();
        $list = $objUserModel->Category();
        $this->RenderView('admin.index', $list);
    }
}