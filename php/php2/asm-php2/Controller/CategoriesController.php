<?php
require_once app_path.'/Model/CategoryModel.php'; // nhúng file model vào để làm việc
class CategoriesController extends ControllerBase{
    public function Index()
    {
        $objUserModel = new CategoryModel(); // tạo đối tượng model
        $data['categories'] = $objUserModel->Category(); //gọi hàm trong model để lấy danh sách
        $this->RenderView('categories.index', $data);
    }

    public function Remove()
    {
        $data =['msg'=>[], 'categories'=>[]];
        $id = $_GET['id'];
        $objUserModel = new CategoryModel();
        $data['msg'] = $objUserModel->removeCategory($id);
        $data['categories'] = $objUserModel->Category();
        $this->RenderView('categories.index', $data);
    }

    public function Edit()
    {
        $data = ['data'=>[], 'msg' => []];
        $objUserModel = new CategoryModel();
        if (isset($_POST['id'])){
            $id = $_POST['id'];
            $name = $_POST['name'];
            $data['msg'] = $objUserModel->updateCategory($id,$name);

        }
        $id = $_GET['id'];
        $data['data'] = $objUserModel->editCategory($id);
        $this->RenderView('categories.edit', $data);

    }

    public function Create()
    {
        $data = ['err'=>[],'msg'=>[],'users'=>[]];
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {
            $category_name = $_POST['category_name'];
            $objUserModel = new CategoryModel();
            $data['msg'] = $objUserModel->createCategory($category_name);
        }
        else{
            $data['err'];
        }
        $this->RenderView('categories.create', $data);
    }
}
