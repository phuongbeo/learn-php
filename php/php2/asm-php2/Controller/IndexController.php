<?php
require_once app_path . '/Model/IndexModel.php'; // nhúng file model vào để làm việc
require_once app_path . '/Sendmail/sendmail.php'; // nhúng file model vào để làm việc
class IndexController extends ControllerBase
{
    public function Index()
    {
        $data =['slides'=>[], 'categories'=>[], 'products'=>[]];
        $objUserModel = new IndexModel();
        $data['slides'] = $objUserModel->Slide();
        $data['categories'] = $objUserModel->Category();
        $data['products'] = $objUserModel->listProducts();
        $this->RenderView('index.view', $data);
    }

    public function showProduct()
    {
        $data =['categories'=>[], 'products'=>[]];
        $id = $_GET['id'];
        $objUserModel = new IndexModel();
        $data['products'] = $objUserModel->loadProduct($id);
        $data['categories'] = $objUserModel->Category();
        $data['slides'] = $objUserModel->Slide();
        $this->RenderView('index.showProduct', $data);

    }

    public function Cart()
    {
        $data =['slides'=>[],'products'=>[],'categories'=>[]];
        $objUserModel = new IndexModel();
        if (isset($_GET['id'])){
            $data['products'] = $objUserModel->cartProduct($_GET['id']);
            $data['categories'] = $objUserModel->Category();
        }
        $data['slides'] = $objUserModel->Slide();
        $this->RenderView('index.cart', $data);
    }

    public function Regis()
    {

        $data = ['err' => [], 'msg' => []];
        $objModel = new IndexModel();
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $pass = $_POST['pass'];
            $fullname = $_POST['fullname'];
            $succes = $objModel->addUser($_POST);
            if ($succes) {
                $subject = 'Lap trinh PHP';
                $to = $email;
                $content = "Chào bạn " . $fullname . "
				Chúc mừng bạn đã đăng kí thành công tài khoản của bạn như sau:
				Username: " . $name . "
				Password: " . $pass . "
				Email: " . $email . "
				Ngày: " . date('Y-m-d');
                $from = 'phuongntph08862@gmail.com';

                $sendMail = Send_email_via_smtp_gmail($subject, $to, $content, array(), array(), $from, 0);

                if ($sendMail['code'] == 1) {
                    // gui mail thanh cong
                    $data['msg'] = $succes;
                    echo $data['msg'];

                } else {
                    echo $sendMail['msg'];
                }
            }
        }else{
            $data['err'];
        }
        $this->RenderView('index.regis', $data);
    }

    public function Login()
    {

        $data = ['err' => [], 'msg' => []];
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $pass = $_POST['pass'];
            $objModel = new IndexModel();
            $userInfo = $objModel->loadLogin($name);
            if (!empty($userInfo)) {
                if (password_verify($pass, $userInfo['pass'])) {
                    unset($userInfo['pass']);

                    if ($_SESSION['auth'] = $userInfo){
                        header('Location:' . base_path . '/?act=index');
                        if ($userInfo['role'] == 1){
                            header('Location:' . base_path . '/?ct=admin&act=index');
                        }
                    }
                } else {
                    $data['err'][] = 'Sai password';
                    $this->RenderView('index.login', $data);
                }

            } else {
                $data['err'][] = 'Không tồn tại tài khoản ' . $name;
                $this->RenderView('index.login', $data);
            }
        } else {
            $this->RenderView('index.login', $data);
        }

    }


    public function Logout()
    {
        if (!empty($_SESSION['auth'])) {
            session_unset();
        }
        header('Location:' . base_path . '/?act=index');
    }

}