<?php
require_once app_path.'/Model/ProductModel.php'; // nhúng file model vào để làm việc
class ProductsController extends ControllerBase{
    public function Index()
    {
        $objUserModel = new ProductModel(); // tạo đối tượng model
        $data['products'] = $objUserModel->listProducts(); //gọi hàm trong model để lấy danh sách
        $this->RenderView('products.index', $data);
    }

    public function Remove()
    {
        $data =['msg'=>[], 'products'=>[]];
        $id = $_GET['id'];
        $objUserModel = new ProductModel();
        $data['msg'] = $objUserModel->removeProduct($id);
        $data['products'] = $objUserModel->listProducts();
        $this->RenderView('products.index', $data);
    }

    public function Edit()
    {
        $data = ['data'=>[], 'msg' => [],'categories'=>[]];
        $objUserModel = new ProductModel();
        if (isset($_POST['id'])){
            $id = $_POST['id'];
            $name = $_POST['name'];
            $code = $_POST['code'];
            $id_category_old = $_POST['id_category_old'];
            $id_category_new = $_POST['id_category_new'];
            $price = $_POST['price'];
            $id_category = "";
            if ($id_category_new){
                $id_category = $id_category_new;
            }
            if ($id_category == ""){
                $id_category = $id_category_old;
            }
            $image_lod = $_POST['image_old'];
            $images = $_FILES['image'];
            $image = "";
            if($images['size'] > 0){ // kiem tra kich co anh
                $filename = uniqid() . "-" . $images['name'];
                move_uploaded_file($images['tmp_name'], 'product-images/' . $filename);
                $image = 'product-images/' . $filename;
            }
            if ($image == ""){
                $image = $image_lod;
            }
            $data['msg'] = $objUserModel->updateProduct($id,$name,$code,$image,$price,$id_category);

        }
        $id = $_GET['id'];
        $data['data'] = $objUserModel->editProduct($id);
        $data['categories'] = $objUserModel->Category();
        $this->RenderView('products.edit', $data);

    }

    public function Create()
    {
        $data = ['err'=>[],'msg'=>[],'users'=>[]];
        $objUserModel = new ProductModel();
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {
            $name = $_POST['name'];
            $code = $_POST['code'];
            $price = $_POST['price'];
            $id_category_new = $_POST['id_category_new'];
            $image = $_FILES['image'];
            if($image['size'] > 0){ // kiem tra kich co anh
                $filename = uniqid() . "-" . $image['name'];
                move_uploaded_file($image['tmp_name'], 'product-images/' . $filename);
                $image = 'product-images/' . $filename;
            }
            $data['msg'] = $objUserModel->createProduct($name,$image,$code,$price,$id_category_new);
        }
        else{
            $data['err'];
        }
        $data['categories'] = $objUserModel->Category();
        $this->RenderView('products.create', $data);
    }
}
