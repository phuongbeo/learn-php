<?php
require_once app_path.'/Model/SlideModel.php'; // nhúng file model vào để làm việc
class SlidesController extends ControllerBase{
    public function Index()
    {
        $objUserModel = new SlideModel(); // tạo đối tượng model
        $data['slides'] = $objUserModel->Slide(); //gọi hàm trong model để lấy danh sách
        $this->RenderView('slides.index', $data);
    }

    public function Remove()
    {
        $data =['msg'=>[], 'slides'=>[]];
        $id = $_GET['id'];
        $objUserModel = new SlideModel();
        $data['msg'] = $objUserModel->removeSlide($id);
        $data['slides'] = $objUserModel->Slide();
        $this->RenderView('slides.index', $data);
    }

    public function Edit()
    {
        $data = ['data'=>[], 'msg' => []];
        $objUserModel = new SlideModel();
        if (isset($_POST['id'])){
            $id = $_POST['id'];
            $title = $_POST['title'];
            $image_lod = $_POST['image_old'];
            $images = $_FILES['image'];
            $image = "";
            if($images['size'] > 0){ // kiem tra kich co anh
                $filename = uniqid() . "-" . $images['name'];
                move_uploaded_file($images['tmp_name'], 'product-images/' . $filename);
                $image = 'product-images/' . $filename;
            }
            if ($image == ""){
                $image = $image_lod;
            }
            $data['msg'] = $objUserModel->updateSlide($id,$image,$title);

        }
        $id = $_GET['id'];
        $data['data'] = $objUserModel->editSlide($id);
        $this->RenderView('slides.edit', $data);

    }

    public function Create()
    {
        $data = ['err'=>[],'msg'=>[],'users'=>[]];
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {
            $title = $_POST['title'];
            $image = $_FILES['image'];
            if($image['size'] > 0){ // kiem tra kich co anh
                $filename = uniqid() . "-" . $image['name'];
                move_uploaded_file($image['tmp_name'], 'product-images/' . $filename);
                $image = 'product-images/' . $filename;
            }
            $objUserModel = new SlideModel();
            $data['msg'] = $objUserModel->createSlide($image,$title);
        }
        else{
            $data['err'];
        }
        $this->RenderView('slides.create', $data);
    }
}
