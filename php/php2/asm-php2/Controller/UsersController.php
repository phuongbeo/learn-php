<?php
require_once app_path.'/Model/UserModel.php'; // nhúng file model vào để làm việc
class UsersController extends ControllerBase{

    public function Index()
    {
        $data = ['users'=>[]];
        $objUserModel = new UserModel();
        $data['users'] = $objUserModel->listUsers();
        $this->RenderView('users.index', $data);
    }

    public function Remove()
    {
        $data =['msg'=>[], 'users'=>[]];
        $id = $_GET['id'];
        $objUserModel = new UserModel();
        $data['msg'] = $objUserModel->removeUser($id);
        $data['users'] = $objUserModel->listUsers();
        $this->RenderView('users.index', $data);
    }

    public function Edit()
    {
        $data = ['data'=>[], 'msg' => []];
        $objUserModel = new UserModel();
        if (isset($_POST['id'])){
            $id = $_POST['id'];
            $name = $_POST['name'];
            $email = $_POST['email'];
            $telephone = $_POST['telephone'];
            $data['msg'] = $objUserModel->updateUser($id,$name,$email,$telephone);

        }
        $id = $_GET['id'];
        $data['data'] = $objUserModel->editUser($id);
        $this->RenderView('users.edit', $data);

    }

    public function Create()
    {
        $data = ['err'=>[],'msg'=>[],'users'=>[]];
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {
            $name = $_POST['name'];
            $pass = $_POST['pass'];
            $fullname = $_POST['fullname'];
            $email = $_POST['email'];
            $telephone = $_POST['telephone'];
            $objUserModel = new UserModel();
            $data['msg'] = $objUserModel->createUser($name,$pass,$email,$fullname,$telephone);
        }
        else{
            $data['err'];
        }
        $this->RenderView('users.create', $data);
    }
}
