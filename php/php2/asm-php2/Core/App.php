
<?php 
	/**
	 * 
	 */
	class App
	{
		public function run()
		{
			//xử lý điều khiển ở đây
			// echo Bắt "<br> Bắt đầu chạy ứng dụng...";
			//VD: link vào web index.php?ct=user;act=list-all
			// index.php?ct=user&act=detail&id=123
			//======================== thu biến:
			$ct = isset($_GET['ct']) ? $_GET['ct'] : 'index';
			$act = isset($_GET['act']) ? $_GET['act'] : 'index';
			
			// echo "<br> Controller: $ct / $act";
			// nhúng controller

			$classCt = str_replace('-', ' ', $ct);
			$classCt = ucwords($classCt);
			$classCt = str_replace(' ', '', $classCt);
			$classCt = $classCt.'Controller';

			//Nhúng file controller
			$file_controller = app_path.'/Controller/'.$classCt.'.php';
			if (file_exists($file_controller)) {
				require_once $file_controller;
			} else {
				die('Không tồn tại file $file_controller');
			}

			// Tạo đối tượng
			$ObjCt = new $classCt();
			// ?ct=san-pham$act=list-all
			//============Xử lí lấy hàm
			$actName = str_replace('-', ' ', $act);
			$actName = ucwords($actName);
			$actName = str_replace(' ', '', $actName);

			if (method_exists($ObjCt, $actName)) {
				$ObjCt->$actName();
			} else {
				die("Không tồn tại action: $actName");
			}
		}

	}
