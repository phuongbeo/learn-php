-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 05, 2020 lúc 03:57 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ontap_php_lab5`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `category_name`) VALUES
(1, 'Iphone'),
(2, 'SamSung'),
(3, 'Xiaomi'),
(4, 'Huawei'),
(5, 'Oppo');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `price` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `code`, `image`, `price`, `id_category`) VALUES
(5, 'Iphone Pro 64GB', 'IP11', 'product-images/iphone11.jpg', 22000000, 1),
(7, 'Iphone X 64 GB', 'IPX', 'product-images/iphoneX_new.jpg', 12000000, 1),
(8, 'Samsung Galaxy S20', 'sss20', 'product-images/samsungS20.jpg', 21500000, 2),
(9, 'Iphone Xsmax 64GB', 'IPXsmax', 'product-images/iphoneXs.jpg', 20000000, 1),
(10, 'Iphone 8 32GB', 'IP8', 'product-images/iphone8.jpg', 17000000, 1),
(11, 'Iphone 7 256GB', 'IP7', 'product-images/iphone7.jpg', 15000000, 1),
(12, 'Iphone6 16GB', 'IP6', 'product-images/iphone6.jpg', 7000000, 1),
(13, 'Samsung Galaxy  S20+ G985', 'SS20PLUS', 'product-images/samsungS20plus.jpg', 24000000, 2),
(14, 'Samsung Galaxy Note 10 Lite', 'SS10', 'product-images/samsung-galaxy-note-10-lite.jpeg', 12500000, 2),
(15, 'Samsung Galaxy A51', 'SSA51', 'product-images/Samsung-Galaxy-A51.jpg', 7290000, 2),
(16, 'Samsung Galaxy A71', 'SSA71', 'product-images/Samsung-Galaxy-A71.jpg', 10900000, 2),
(17, 'Samsung Galaxy A30s', 'SSA30s', 'product-images/Samsung-Galaxy-A30s.png', 5190000, 2),
(18, 'Xiaomi Note 10 Pro', 'XM10', 'product-images/xiaomi-note-10-pro.jpg', 14000000, 3),
(19, 'Xiaomi Mi 9 Lite', 'XMm9', 'product-images/Xiaomi-Mi-9-Lite.jpg', 8500000, 3),
(20, 'Xiaomi Redmi Note 8 Pro', 'XMN8', 'product-images/xiaomi-redmi-note-8-pro.jpg', 5500000, 3),
(21, 'Xiaomi Redmi Note 7', 'XMN7', 'product-images/xiaomi-redmi-note-7.jpeg', 4290000, 3),
(22, 'Xiaomi Mi A3', 'XMA3', 'product-images/xiaomi-mi-a-3.jpg', 3790000, 3),
(23, 'Xiaomi Redmi 7A', 'XM7A', 'product-images/xiaomi-redmi-7a.jpg', 2500000, 3),
(24, 'Huawe Mate 30 Pro', 'HWM30', 'product-images/huawe_mate30_pro.jpg', 20990000, 4),
(25, 'Huawei Nova 5T', 'HW5T', 'product-images/huawei-nova-5t.jpeg', 8500000, 4),
(26, 'Huawei Nova 3I', 'HWN3I', 'product-images/huawei-nova-3i.jpg', 5990000, 4),
(27, 'Huawei Y9s', 'HWY9', 'product-images/huawei-Y9s.jpg', 5000000, 4),
(28, 'Huawei P30 Pro', 'HWP30', 'product-images/huawei-p30-pro.jpg', 5490000, 4),
(29, 'Huawei Y7 Pro', 'HWY7', 'product-images/Huawei-Y7-Pro.jpg', 3490000, 4),
(30, 'Oppo Reno 10x Zoom', 'OPR10', 'product-images/oppo-reno-10x-zoom.jpeg', 14690000, 5),
(31, 'Oppo Reno2 F', 'OPR2', 'product-images/OPPO-Reno2-f.jpg', 7990000, 5),
(32, 'Oppo A31', 'OPA31', 'product-images/oppo-a31.jpg', 4490000, 5),
(33, 'Oppo F11 Pro', 'OPF11', 'product-images/oppo-f11-pro.jpeg', 6490000, 5),
(34, 'Oppo A9', 'OPA9', 'product-images/oppo-a9.jpg', 5990000, 5),
(35, 'Oppo A5', 'OPA5', 'product-images/oppo-a5.jpeg', 4790000, 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slides`
--

INSERT INTO `slides` (`id`, `image`, `title`) VALUES
(1, 'product-images/slide1.jpg', 'slide show 1'),
(2, 'product-images/slide2.jpg', 'slide show 2'),
(3, 'product-images/slide3.jpg', 'slide show 3');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `fullname` varchar(165) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` int(11) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `pass`, `fullname`, `email`, `telephone`, `role`) VALUES
(24, 'phuongnguyen', '$2y$10$RIaclI2x.jN8G4/wUSLLm.v72RIMk28hw/vK3hzKIuhbOIn9hzJOu', 'Phuong Nguyen', 'phuongnguyen@gmail.com', 974667645, 1),
(31, 'thanhnguyen', '$2y$10$CxpFayK37eav5UQouxhY5erPYv2.U.p1EV92Wr7pvkumiD3hJwyKu', 'Thanh Nguyen', 'thanhnguyen9595@gmail.com', 377713817, 0),
(52, 'caothinguyet', '$2y$10$TyCk2ogfxh0QmOTKkmMUUOeTkhbn80CT.gc1svUx0IQB4gqfjUo86', 'Cao Thi Nguyet', 'caothinguyet@gmail.com', 974667645, 0),
(53, 'nguyenvanbinh', '$2y$10$ANrsKl9o.zpz1qXpphUA0u4jns.zkvFVI8MDrQoe0L9ZjMLRcfKde', 'Phuong Nguyen', 'nguyenvanbinh@gmail.com', 965894723, 0),
(54, 'dungcao', '$2y$10$xC2w2dIloFrj3/hjFsoAj.BewD1FeGmgYkhCaxiF7Ws5vt8vxbqhC', 'dung cao', 'dungcaond@gmail.com', 965894598, 0),
(55, 'nguyenvana', '$2y$10$j5y0Bi06Atp9V4QXz4Gy5udgQxMP7xFi8TO7gkj9UrFln7C/4ifnG', 'nguyen van a', 'nguyenbinh@gmail.com', 974667645, 0),
(56, 'nguyenvanb', '$2y$10$Wx31nSnijN1hgQCOUKmWc.UB6bWIm2T7uCR21T8ji6mWcylMiSjk2', 'nguyen van b', 'nguyenvanb@gmail.com', 974667645, 0),
(57, 'nguyenvanabc', '$2y$10$leSxdQqxA18EA5CAKxYrGekYTQfs7GWz1J0VT7TFignKMxwUPAVF2', 'nguyen van c', 'nguyenvanc@gmail.com', 988695594, 0);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`code`);

--
-- Chỉ mục cho bảng `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT cho bảng `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
