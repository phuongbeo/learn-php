<?php

class AdminModel extends DB
{
    private $categories = "categories";

    public function Category(){
        $sql = "SELECT * FROM $this->categories";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }
}