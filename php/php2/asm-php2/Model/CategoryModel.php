<?php

class CategoryModel extends DB
{
    private $categories = "categories";

    public function Category(){
        $sql = "SELECT * FROM $this->categories";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function removeCategory($id){
        $sql = "DELETE FROM $this->categories WHERE id = $id";
        $res = $this->Query($sql);
        if (isset($res)){
            return "Delete Successful";
        }
    }

    public function editCategory($id){
        $sql = "SELECT * FROM $this->categories WHERE id = $id";
        $res = $this->Query($sql);
        $data = null;
        while ($row = $res->fetch_assoc()) {
            $data = $row;
        }
        return $data;
    }

    public function updateCategory($id,$name){
        $sql = "UPDATE $this->categories SET category_name = '$name' WHERE id = $id";
        $res = $this->Query($sql);
        if (isset($res)){
            return "Update Successful";
        }
    }

    public function createCategory($category_name){

        $sql = "SELECT * FROM  $this->categories WHERE category_name = '$category_name'";
        $resName = $this->Query($sql);
        if ($resName->num_rows == 1) {
            return "Tên danh mục đã tồn tại";
        } else {
            $sql = "INSERT INTO $this->categories (category_name) VALUES ('$category_name')";
            $res = $this->Query($sql);
            if (isset($res)){
                return "Create Successful";
            }
        }
    }
}