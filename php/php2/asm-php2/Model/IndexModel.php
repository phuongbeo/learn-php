<?php

class IndexModel extends DB
{
    private $users = "users";
    private $categories = "categories";
    private $products = "products";
    private $slides = "slides";

    public function Category(){
        $sql = "SELECT * FROM $this->categories";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function Slide(){
        $sql = "SELECT * FROM $this->slides ORDER BY id ASC";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function listProducts(){
        $sql = "SELECT * FROM $this->products";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function cartProduct($id){
        $sql = "SELECT * FROM $this->products WHERE id = $id";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function loadProduct($id){
        if(isset($id)){
            $sql = "SELECT * FROM $this->categories, $this->products WHERE $this->categories.id = ($this->products.id_category = $id)";
            $res = $this->Query($sql);
            $data = [];
            while ($row = $res->fetch_assoc()) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function addUser($post)
    {

        $name = $post['name'];
        $pass = $post['pass'];
        $cp = $post['cp'];
        $fullname = $post['fullname'];
        $email = $post['email'];
        $phone = $post['phone'];

        $check = '/^\w{6,20}$/';
        $checkcp = '/^([a-zA-Z])\w{7,14}$/';
        $checkfn = '/^[a-zA-Z]$/';
        $regex_phone = '/((09|03|07|08|05)+([0-9]{8})\b)/';
        $regex_email = '/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';

        $sql = "SELECT * FROM  $this->users WHERE name = '$name'";
        $resName = $this->Query($sql);
        $sqle = "SELECT * FROM  $this->users WHERE email = '$email'";
        $resEmail = $this->Query($sqle);
        if (!preg_match($check, $name)) {
            return "Tên tài khoản gồm ký tự từ a-z hoặc A-Z hoặc 0-9 tối đa 6 - 20 ký tự";
        } else if (!preg_match($checkcp, $cp)) {
            return "Mật khẩu từ 8 đến 14 ký tự";
        } else if (!preg_match($regex_email, $email)) {
            return "Email sai định dạng";
        } else if ($resName->num_rows == 1) {
            return "Tên tài khoản bị đã tồn tại";
        } else if ($resEmail->num_rows == 1) {
            return "Email đã được đăng ký";
        } else if ($pass != $cp) {
            return "Mật khẩu nhập lại sai";
        } else if (!preg_match($regex_phone, $phone)) {
            return "Số điện thoại sai định dạng";
        } else if (!preg_match($checkfn, $fullname)) {
            $pass = password_hash($cp, PASSWORD_DEFAULT);
            $sql = "INSERT INTO $this->users (name, pass, fullname, email, telephone, role) VALUES ('$name', '$pass', '$fullname', '$email', '$phone', 0)";
            $res = $this->Query($sql);
            if ($res) {
                return "Đăng ký thành công...!";
            }
        }
    }

    public function loadLogin($name)
    {
        $sql = "SELECT * FROM  $this->users WHERE name = '{$name}'";
        $res = $this->Query($sql);
        if ($res->num_rows == 1) {
            $user = $res->fetch_assoc();
            return $user;
        }
        return null;
    }
}