<?php

class ProductModel extends DB
{
    private $products = "products";
    private $categories = "categories";

    public function listProducts(){
        $sql = "SELECT * FROM $this->products";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function removeProduct($id){
        $sql = "DELETE FROM $this->products WHERE id = $id";
        $res = $this->Query($sql);
        if (isset($res)){
            return "Delete Successful";
        }
    }

    public function editProduct($id){
        $sql = "SELECT * FROM $this->products WHERE id = $id";
        $res = $this->Query($sql);
        $data = null;
        while ($row = $res->fetch_assoc()) {
            $data = $row;
        }
        return $data;
    }

    public function updateProduct($id,$name,$code,$image,$price,$id_category){
        $sql = "UPDATE $this->products SET name = '$name',
                                    code = '$code',
                                    image = '$image',
                                    price = '$price',
                                    id_category = '$id_category'
                                     WHERE id = $id";
        $res = $this->Query($sql);
        if (isset($res)){
            return "Update Successful";
        }
    }

    public function Category(){
        $sql = "SELECT * FROM $this->categories";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function createProduct($name,$image,$code,$price,$id_category_new){

        $sql = "SELECT * FROM  $this->products WHERE image = '$image'";
        $resImage = $this->Query($sql);
        $sqle = "SELECT * FROM  $this->products WHERE name = '$name'";
        $resName = $this->Query($sqle);
        $sqle = "SELECT * FROM  $this->products WHERE code = '$code'";
        $resCode = $this->Query($sqle);
        if ($resImage->num_rows == 1) {
            return "Image đã tồn tại";
        } else if ($resName->num_rows == 1) {
            return "Name đã tồn tại";
        }else if ($resCode->num_rows == 1) {
            return "Code đã tồn tại";
        } else {
            $sql = "INSERT INTO $this->products (name, image, code, price, id_category) VALUES ('$name', '$image', '$code', '$price', '$id_category_new')";
            $res = $this->Query($sql);
            if (isset($res)){
                return "Create Successful";
            }
        }
    }
}