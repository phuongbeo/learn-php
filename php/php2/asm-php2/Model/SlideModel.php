<?php

class SlideModel extends DB
{
    private $slides = "slides";

    public function Slide(){
        $sql = "SELECT * FROM $this->slides ORDER BY id ASC";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function removeSlide($id){
        $sql = "DELETE FROM $this->slides WHERE id = $id";
        $res = $this->Query($sql);
        if (isset($res)){
            return "Delete Successful";
        }
    }

    public function editSlide($id){
        $sql = "SELECT * FROM $this->slides WHERE id = $id";
        $res = $this->Query($sql);
        $data = null;
        while ($row = $res->fetch_assoc()) {
            $data = $row;
        }
        return $data;
    }

    public function updateSlide($id,$image,$title){
        $sql = "UPDATE $this->slides SET image = '$image',
                                    title = '$title'
                                     WHERE id = $id";
        $res = $this->Query($sql);
        if (isset($res)){
            return "Update Successful";
        }
    }

    public function createSlide($image,$title){

        $sql = "SELECT * FROM  $this->slides WHERE image = '$image'";
        $resImage = $this->Query($sql);
        $sqle = "SELECT * FROM  $this->slides WHERE title = '$title'";
        $resTitle = $this->Query($sqle);
        if ($resImage->num_rows == 1) {
            return "Image đã tồn tại";
        } else if ($resTitle->num_rows == 1) {
            return "Title đã tồn tại";
        } else {
            $sql = "INSERT INTO $this->slides (image, title) VALUES ('$image', '$title')";
            $res = $this->Query($sql);
            if (isset($res)){
                return "Create Successful";
            }
        }
    }
}