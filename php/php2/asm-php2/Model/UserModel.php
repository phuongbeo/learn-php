<?php

/**
 *
 */
class UserModel extends DB
{

    private $users = "users";

    public function listUsers(){
        $sql = "SELECT * FROM $this->users";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function removeUser($id){
        $sql = "DELETE FROM $this->users WHERE id = $id";
        $res = $this->Query($sql);
        if (isset($res)){
            return "Delete Successful";
        }
    }

    public function editUser($id){
        $sql = "SELECT * FROM $this->users WHERE id = $id";
        $res = $this->Query($sql);
        $user = null;
        while ($row = $res->fetch_assoc()) {
            $user = $row;
        }
        return $user;
    }

    public function updateUser($id,$name,$email,$telephone){
        $sql = "UPDATE $this->users SET name = '$name',
                                    email = '$email', 
                                    telephone = '$telephone'
                                     WHERE id = $id";
        $res = $this->Query($sql);
        if (isset($res)){
            return "Update Successful";
        }
    }

    public function createUser($name,$pass,$email,$fullname,$telephone){

        $check = '/^\w{6,20}$/';
        $checkcp = '/^([a-zA-Z])\w{7,14}$/';
        $checkfn = '/^[a-zA-Z]$/';
        $regex_phone = '/((09|03|07|08|05)+([0-9]{8})\b)/';
        $regex_email = '/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';

        $sql = "SELECT * FROM  $this->users WHERE name = '$name'";
        $resName = $this->Query($sql);
        $sqle = "SELECT * FROM  $this->users WHERE email = '$email'";
        $resEmail = $this->Query($sqle);
        if (!preg_match($check, $name)) {
            return "Tên tài khoản gồm ký tự từ a-z hoặc A-Z hoặc 0-9 tối đa 6 - 20 ký tự";
        } else if (!preg_match($checkcp, $pass)) {
            return "Mật khẩu từ 8 đến 14 ký tự";
        } else if (!preg_match($regex_email, $email)) {
            return "Email sai định dạng";
        } else if ($resName->num_rows == 1) {
            return "Tên tài khoản bị đã tồn tại";
        } else if ($resEmail->num_rows == 1) {
            return "Email đã được đăng ký";
        } else if (!preg_match($regex_phone, $telephone)) {
            return "Số điện thoại sai định dạng";
        } else if (!preg_match($checkfn, $fullname)) {
            $pass = password_hash($pass, PASSWORD_DEFAULT);
            $sql = "INSERT INTO $this->users (name, pass, fullname, email, telephone, role) VALUES ('$name', '$pass', '$fullname', '$email', '$telephone', 0)";
            $res = $this->Query($sql);
            if (isset($res)){
                return "Create Successful";
            }
        }
    }
}
