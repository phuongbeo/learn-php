<div class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <!-- BEGIN: Header -->
        <?php include_once('components/header-admin.php') ?>
        <!-- END: Header -->
        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
                <i class="la la-close"></i>
            </button>
            <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
                <!-- BEGIN: Aside Menu -->
                <div
                        id="m_ver_menu"
                        class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                        m-menu-vertical="1"
                        m-menu-scrollable="0" m-menu-dropdown-timeout="500"
                >
                    <?php
                    include_once('components/menu.php');
                    ?>
                </div>
                <!-- END: Aside Menu -->
            </div>
            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <!-- END: Subheader -->
                <div class="m-content">
                    <div class="row">
                        <div class="col-xl-12">
                            <img src="assets/img/slide4.jpg" alt="" style="width: 100%;height: 100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Body -->
        <!-- begin::Footer -->
        <?php include_once('components/footer.php') ?>
        <!-- end::Footer -->
    </div>
    <!--end::Base Scripts -->
</div>
<!-- end::Body -->
