<?php
$data = $this->data;
$productByCode = $data['products'];
//echo "<pre>";
//var_dump($productByCode[0]['code']);
//exit();
if (!empty($_GET["action"])) {
    switch ($_GET["action"]) {
        case "add":
            if (!empty($_POST["quantity"])) {
                $itemArray = array($productByCode[0]["code"] => array('id' => $productByCode[0]["id"],
                    'name' => $productByCode[0]["name"],
                    'code' => $productByCode[0]["code"],
                    'quantity' => $_POST["quantity"],
                    'price' => $productByCode[0]["price"],
                    'image' => $productByCode[0]["image"]));

                if (!empty($_SESSION["cart_item"])) {
                    if (in_array($productByCode[0]["code"], array_keys($_SESSION["cart_item"]))) {
                        foreach ($_SESSION["cart_item"] as $k => $v) {
                            if ($productByCode[0]["code"] == $k) {
                                if (empty($_SESSION["cart_item"][$k]["quantity"])) {
                                    $_SESSION["cart_item"][$k]["quantity"] = 0;
                                }
                                $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                            }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }
            }
            break;
        case "remove":
            if (!empty($_SESSION["cart_item"])) {
                foreach ($_SESSION["cart_item"] as $k => $v) {
                    if ($_GET["code"] == $k)
                        unset($_SESSION["cart_item"][$k]);
                    if (empty($_SESSION["cart_item"]))
                        unset($_SESSION["cart_item"]);
                }
            }
            break;
        case "empty":
            unset($_SESSION["cart_item"]);
            break;
    }
}
?>
<?php include_once('layout/nav.php') ?>
<div class="row mt-5">
    <div class="col-sm-12">
        <?php include_once('layout/slide.php') ?>
    </div>
</div>
<div class="container">
    <div id="shopping-cart">
        <div class="txt-heading">Shopping Cart</div>
        <a id="btnEmpty" href="?act=cart&action=empty">Empty Cart</a>
        <a class="btn btn-outline-success mt-2" href="?act=index">Continue Shopping</a>
        <?php
        if (isset($_SESSION["cart_item"])) {
            $total_quantity = 0;
            $total_price = 0;
            ?>
            <table class="tbl-cart" cellpadding="10" cellspacing="1">
                <tbody>
                <tr>
                    <th style="text-align:left;">Name</th>
                    <th style="text-align:left;">Code</th>
                    <th style="text-align:right;" width="5%">Quantity</th>
                    <th style="text-align:right;" width="10%">Unit Price</th>
                    <th style="text-align:right;" width="10%">Price</th>
                    <th style="text-align:center;" width="5%">Remove</th>
                </tr>
                <?php
                foreach ($_SESSION["cart_item"] as $item) {
                    $item_price = $item["quantity"] * $item["price"];
                    ?>
                    <tr>
                        <td><img src="<?php echo $item["image"]; ?>" style="width: 50px;height: 50px;"
                                 class="cart-item-image"/><?php echo $item["name"]; ?>
                        </td>
                        <td><?php echo $item["code"]; ?></td>
                        <td style="text-align:right;"><?php echo $item["quantity"]; ?></td>
                        <td style="text-align:right;"><?php echo number_format($item["price"]) . "<sup>" . "đ" . "</sup>"; ?></td>
                        <td style="text-align:right;"><?php echo number_format($item_price) . "<sup>" . "đ" . "</sup>"; ?></td>
                        <td style="text-align:center;">
                            <a href="?act=cart&action=remove&code=<?php echo $item["code"]; ?>" class="btnRemoveAction">
                                <img src="Public/img/icon-delete.png" alt="Remove Item"/>
                            </a>
                        </td>
                    </tr>
                    <?php
                    $total_quantity += $item["quantity"];
                    $total_price += ($item["price"] * $item["quantity"]);
                }
                ?>

                <tr>
                    <td colspan="2" align="right">Total:</td>
                    <td align="right"><?php echo $total_quantity; ?></td>
                    <td align="right" colspan="2">
                        <strong><?php echo number_format($total_price) . "<sup>" . "đ" . "</sup>"; ?></strong>
                    </td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="no-records">Your Cart is Empty</div>
            <?php
        }
        ?>
    </div>
</div>
<?php include_once('layout/footer.php') ?>