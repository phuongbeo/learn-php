<?php include_once('layout/nav.php') ?>
<div class="container">
    <div class="row justify-content-center" style="margin-top: 100px">
        <div class="col-sm-5">
            <div class="card">
                <div class="card-body">
                    <h1 class="text-center text-info">Login</h1>
                    <form action="" enctype="multipart/form-data" method="post" role="form"
                          onsubmit="return validateForm();">
                        <?php
                        if (!empty($this->data['err'])) {
                            echo '<p class="err">' . implode('<br>', $this->data['err']) . '</p>';
                        }
                        if (!empty($this->data['msg'])) {
                            echo '<p class="msg">' . implode('<br>', $this->data['msg']) . '</p>';
                        }

                        ?>
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" class="form-control data" name="name" placeholder="Enter username">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control data" name="pass" placeholder="Enter passwoord">
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-danger">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('layout/footer-mt.php') ?>
<script>
    function validateForm() {
        var i;
        var control = document.getElementsByClassName('data');
        var length = document.getElementsByClassName('data').length;

        for (i = 0; i < length; i++) {
            var data = control[i].value;
            if (data == '') {
                alert('Bạn cần nhập đầy đủ giá trị!');
                return false;
            }
        }

    }
</script>
