<?php
$data = $this->data;
?>
<?php include_once('layout/nav.php') ?>
<div class="container">
    <div class="row justify-content-center" style="margin-top: 100px">
        <div class="col-sm-5">
            <?php
            if (($data['msg']))
            {
                echo '<div class="col-12 alert alert-danger">' . $data['msg'] . '</div>';
            }
            if (($data['err']))
            {
                echo '<div class="col-12 alert alert-danger">' . $data['err'] . '</div>';
            }
            ?>
            <div class="card">
                <div class="card-body">
                    <h1 class="text-center text-info">Register</h1>
                    <form action="" enctype="multipart/form-data" method="post" role="form"
                          onsubmit="return validateForm();">
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" name="name" placeholder="Name" class="form-control data">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="pass" placeholder="Password" id="pass" class="form-control data">
                        </div>
                        <div class="form-group">
                            <label for="">Confirm Password</label>
                            <input type="password" name="cp" placeholder="Confirm Password" id="passcp" class="form-control data">
                        </div>
                        <div class="form-group">
                            <label for="">Full Name</label>
                            <input type="text" name="fullname" placeholder="Full Name" class="form-control data">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" name="email" placeholder="Email" class="form-control data">
                        </div>
                        <div class="form-group">
                            <label for="">Telephone</label>
                            <input type="text" name="phone" placeholder="Telephone" class="form-control data">
                        </div>

                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-danger">Register</button>
                            <input type="reset" class="btn btn-primary" name="reset" value="Reset">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('layout/footer-mt.php') ?>
<script language="javascript">
    function validateForm() {
        var i;
        var control = document.getElementsByClassName('data');
        var length = document.getElementsByClassName('data').length;

        for (i = 0; i < length; i++) {
            var data = control[i].value;
            if (data == '') {
                alert('Bạn cần nhập đầy đủ giá trị!');
                return false;
            }
        }

    }
</script>