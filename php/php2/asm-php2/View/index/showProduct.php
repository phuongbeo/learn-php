<?php
$data = $this->data;
?>
<?php include_once('layout/nav.php') ?>
    <div class="row mt-5">
        <div class="col-sm-12">
            <?php include_once('layout/slide.php') ?>
        </div>
    </div>
    <div class="container">
        <div id="product-grid">
            <div class="txt-heading">List of products</div>
            <div class="row">
                <div class="col-sm-10">
                    <?php
                    foreach ($data['products'] as $key => $value) {
                        ?>
                        <div class="product-item">
                            <form method="post" action="?act=cart&action=add&id=<?php echo $value["id"]; ?>">
                                <div class="product-image mt-2"><img src="<?php echo $value["image"]; ?>"></div>
                                <div class="product-tile-footer mb-2">
                                    <div class="text-center my-2"><?php echo $value["name"]; ?></div>
                                    <div class="product-price"><?php echo number_format($value["price"]) . "<sup>" . "đ" . "</sup>"; ?></div>
                                    <div class="cart-action">
                                        <input style="width: 50px" type="text"
                                               class="product-quantity btn btn-outline-warning" name="quantity"
                                               value="1" size="2"/>
                                        <button type="submit" name="submit" class="btnAddAction btn btn-outline-danger">
                                            Add cart
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-sm-2">
                    <h4 class="text-danger mb-4">Brand</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="list-group">
                                <?php foreach ($data['categories'] as $u): ?>
                                    <a href="?act=showProduct&id=<?php echo $u['id']; ?>&category=<?php echo $u['category_name']; ?>"
                                       class="list-group-item btn-outline-info">
                                        <b><?php echo $u['category_name'] ?></b>
                                    </a>
                                <?php endforeach ?>
                            </div>
                        </div>
                        <?php include_once('layout/news.php') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include_once('layout/footer.php') ?>