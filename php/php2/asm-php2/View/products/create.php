<?php
$data = $this->data;
?>
<div class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <!-- BEGIN: Header -->
        <?php include_once('components/header-admin.php') ?>
        <!-- END: Header -->
        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
                <i class="la la-close"></i>
            </button>
            <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
                <!-- BEGIN: Aside Menu -->
                <div
                    id="m_ver_menu"
                    class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                    m-menu-vertical="1"
                    m-menu-scrollable="0" m-menu-dropdown-timeout="500"
                >
                    <?php
                    include_once('components/menu.php');
                    ?>
                </div>
                <!-- END: Aside Menu -->
            </div>
            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <!-- BEGIN: Subheader -->
                <!-- END: Subheader -->
                <div class="m-content">
                    <?php
                    if (($data['msg']))
                    {
                        echo '<div class="col-5 alert alert-danger">' . $data['msg'] . '</div>';
                    }
                    if (($data['err']))
                    {
                        echo '<div class="col-5 alert alert-danger">' . $data['err'] . '</div>';
                    }
                    ?>
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Create Product
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <!--begin::Section-->
                            <div class="m-section">
                                <div class="m-section__content">
                                    <div class="row">
                                        <!-- ============================================================== -->
                                        <!-- basic table  -->
                                        <!-- ============================================================== -->
                                        <div class="col-md-6">
                                            <form action="" method="post" enctype="multipart/form-data">
                                                <div>
                                                    <label for="">Name:</label>
                                                    <input type="text" name="name" placeholder="Enter Name" class="form-control">
                                                </div>
                                                <br>
                                                <div>
                                                    <label for="">Code:</label>
                                                    <input type="text" name="code" placeholder="Enter Code" class="form-control">
                                                </div>
                                                <br>
                                                <div>
                                                    <label for="">Image:</label><br>
                                                    <input type="file" name="image">
                                                </div>
                                                <br>
                                                <div>
                                                    <label for="">Price:</label>
                                                    <input type="text" name="price" placeholder="Enter Price" class="form-control">
                                                </div>
                                                <br>
                                                <div>
                                                    <label for="">Category: </label>
                                                    <select name="id_category_new" class="form-control" >
                                                        <option value="#">---Select Category---</option>
                                                        <?php foreach ($data['categories'] as $item): ?>
                                                            <option value="<?php echo $item['id']?>"><?php echo $item['category_name']?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <br>
                                                <div>
                                                    <button type="submit" name="submit" class="btn btn-danger">Create</button>
                                                    <a href="?ct=products&act=index" class="btn btn-info" role="button">Cancel</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Body -->
        <!-- begin::Footer -->
        <?php include_once('components/footer.php') ?>
        <!-- end::Footer -->
    </div>
    <!--end::Base Scripts -->
</div>
<!-- end::Body -->
