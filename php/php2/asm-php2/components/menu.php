<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
    <li class="m-menu__item  m-menu__item--submenu m-menu__item--open m-menu__item--expanded"
        aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="?ct=users&act=index" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-avatar"></i>
            <span class="m-menu__link-text">
										Users
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="?ct=products&act=index" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-share"></i>
            <span class="m-menu__link-text">
										Products
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="?ct=categories&act=index" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-list"></i>
            <span class="m-menu__link-text">
										Categories
									</span>
        </a>
    </li>
    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
        <a href="?ct=slides&act=index" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-interface-7"></i>
            <span class="m-menu__link-text">
										Slides
									</span>
        </a>
    </li>
</ul>