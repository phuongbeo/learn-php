<div class="footer mt-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-3  col-md-4 col-sm-4">
                <div class="footer_dv text-warning">
                    <h4>Images</h4>

                    <img class="img-responsive" src="https://cdn.fptshop.com.vn/Uploads/Originals/2019/9/11/637037687763926758_11-pro-max-xanh.png" alt="about" style="width: 250px;height: 250px">
                    <p></p>
                    <p>Latest smartphone at our store ...!</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="footer_dv text-success">
                    <h4>Categories</h4>
                    <div class="line-dash"></div>
                    <ul>
                        <li><a class="a-font" href="#">Apple</a></li>
                        <li><a class="a-font" href="#">Samsung</a></li>
                        <li><a class="a-font" href="#">Xiaomi</a></li>
                        <li><a class="a-font" href="#">Huawei</a></li>
                        <li><a class="a-font" href="#">Oppo</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="footer_dv text-success">
                    <h4>All Pages</h4>
                    <div class="line-dash"></div>
                    <ul>
                        <li><a class="a-font" href="?act=index">Home</a></li>
                        <li><a class="a-font" href="?act=cart">Cart</a></li>
                        <li><a class="a-font" href="?act=login">Login</a></li>
                        <li><a class="a-font" href="?act=regis">Register</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <div class="footer_dv text-success">
                    <h4>Contact us</h4>
                    <ul class="list-unstyled mb-0">
                        <li><i class="fas fa-map-marker-alt fa-2x"></i>
                            <p>116 Trung Kính</p>
                        </li>
                        <li><i class="fas fa-phone fa-2x"></i>
                            <p>+ 0974667645</p>
                        </li>
                        <li><i class="fas fa-envelope fa-2x"></i>
                            <p>smartphone@gmail.com</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
</footer>