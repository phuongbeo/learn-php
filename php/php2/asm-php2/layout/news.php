<div class="col-sm-12">
    <div class="row">
        <div class="col-12 pt-3">
            <h4 class="text-primary">News</h4>
        </div>
    </div>
    <div class="row py-2">
        <div class="col-11 col-md-9 col-lg-10 pl-1 pl-md-2">
            <div class="breaking-box pt-2 pb-1">
                <!--marque-->
                <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseleave="this.start();">
                    <a class="h6 font-weight-light" href="https://www.thegioididong.com/tin-tuc/iphone-the-he-tiep-theo-duoc-tich-hop-logo-tao-phat-sang-1203041"><span class="position-relative mx-2 badge badge-primary rounded-0">Apple</span> iPhone thế hệ tiếp theo có thể được tích hợp logo táo phát sáng</a>
                    <a class="h6 font-weight-light" href="https://www.thegioididong.com/tin-tuc/covid-19-khien-ke-hoach-san-xuat-chip-3nm-samsung-bi-cham-tre-1247354"><span class="position-relative mx-2 badge badge-primary rounded-0">Samsung</span> Covid-19 khiến kế hoạch sản xuất chip 3nm của Samsung bị chậm trễ</a>
                    <a class="h6 font-weight-light" href="https://www.thegioididong.com/tin-tuc/redmi-9-va-con-duong-tro-lai-cua-ong-trum-gia-re-1247184"><span class="position-relative mx-2 badge badge-primary rounded-0">Xiaomi</span> Redmi 9 và con đường trở lại của ông trùm giá rẻ</a>
                    <a class="h6 font-weight-light" href="https://www.thegioididong.com/tin-tuc/su-kien/su-kien-ra-mat-huawei-nova-7-346"><span class="position-relative mx-2 badge badge-primary rounded-0">Huawei</span> Sự kiện ra mắt Huawei nova 7</a>
                    <a class="h6 font-weight-light" href="https://www.thegioididong.com/tin-tuc/oppo-a12e-gia-re-bong-dung-xuat-hien-tren-trang-chu-oppo-1246452"><span class="position-relative mx-2 badge badge-primary rounded-0">Oppo</span> OPPO A12e giá rẻ bỗng dưng xuất hiện trên trang chủ OPPO Việt Nam</a>
                </marquee>
            </div>
        </div>
    </div>
    <div class="row my-3">
        <img src="https://images.fpt.shop/unsafe/fit-in/465x465/filters:quality(90):fill(white)/cdn.fptshop.com.vn/Uploads/Originals/2019/9/11/637037687765081535_11-pro-max-vang.png" alt="" style="width: 100%;height: auto">
    </div>
    <div class="row mb-4">
        <div class="col-12 text-center">
            <p>News of the product <a target="_blank" href="https://bootstrap.news/bootstrap-4-template-news-portal-magazine/">Smartphone</a> | Design by Programmer</p>
        </div>
    </div>
</div>