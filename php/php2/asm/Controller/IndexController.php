<?php
	require_once app_path.'/Model/UserModel.php'; // nhúng file model vào để làm việc
	require_once app_path.'/Sendmail/sendmail.php'; // nhúng file model vào để làm việc
	class IndexController extends ControllerBase{
	public function Index(){
		return $this;
	}
    public function showProduct(){
        $objUserModel = new UserModel(); // tạo đối tượng model
        $showProduct = $objUserModel->loadProduct(); //gọi hàm trong model để lấy danh sách
        $this->RenderView('index.showProduct', $showProduct);
	    
    }

	public function View(){
        $objUserModel = new UserModel(); // tạo đối tượng model
        $data = $objUserModel->Slide(); //gọi hàm trong model để lấy danh sách
        $this->RenderView('index.view', $data);
    }
    
    public function listProduct(){
        $objUserModel = new UserModel();
        $list = $objUserModel->Category();
        $this->RenderView('index.view', $list);
    }
	public function Regis(){

		$data =['err'=>[], 'msg'=>[]];
		$objModel = new UserModel();
		if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])){	
				$name = $_POST['name'];
				$email = $_POST['email'];
				$pass = $_POST['pass'];
				$fullname = $_POST['fullname'];
			$succes = $objModel->addUser($_POST);
				if ($succes) {
					$url = $objModel->curPageURL();
				$subject ='Lap trinh PHP';
				$to = $email;
				$content ="Chào bạn ".$fullname."
				Chúc mừng bạn đã đăng kí thành công tài khoản của bạn như sau:
				Username: ".$name."
				Password: ".$pass."
				Email: ".$email."
				Ngày: ".date('Y-m-d');
				$from ='phuongntph08862@gmail.com';

				$sendMail = Send_email_via_smtp_gmail($subject, $to, $content, array(), array(), $from, 0);

				if ($sendMail['code'] == 1) {
				    // gui mail thanh cong
				    $data['msg'] = $succes;	
				    echo $data['msg'];
				   
				} else {
				    echo $sendMail['msg'];
				}
			}
		}	
		$this->RenderView('user.regis', $data);
	}

	public function Login(){

		$data =['err'=>[], 'msg'=>[] ];
		if(isset($_POST['submit'])){					
			$name = $_POST['name'];
			$pass = $_POST['pass'];
			$objModel = new UserModel();
			$userInfo = $objModel->loadLogin($name);
		if(!empty($userInfo)){
			if(password_verify($pass, $userInfo['pass'])) {
				unset($userInfo['pass']);

				$_SESSION['auth'] = $userInfo;
//				$this->RenderView('index.login', $data);
                header('Location:'.base_path .'/?act=view');
				}else{
				$data['err'][]= 'Sai password';
				$this->RenderView('index.login', $data)	;
				}

		}else{
			$data['err'][]= 'Không tồn tại tài khoản '.$name;
		$this->RenderView('index.login', $data);
			}
		}else {
		$this->RenderView('index.login', $data);
		}	

	}



	public function Logout(){
		if(!empty($_SESSION['auth'])){
			session_unset();
		}
		header('Location:'.base_path .'/?act=view');
		}

}