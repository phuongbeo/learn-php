<?php

/**
 *
 */
class UserModel extends DB
{

    private $customer = "customer";
    private $category = "category";
    private $slide = "slide";

    public function loadProduct(){
        $id = $_GET['id'];
        $sql = "select * from category, tblproduct where category.id = (tblproduct.id_category = $id)";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }
    public function Category(){
        $sql = "SELECT * FROM $this->category";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }
    public function Slide(){
        $sql = "SELECT * FROM $this->slide ORDER BY id ASC";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }
    public function addUser($post)
    {

        $name = $post['name'];
        $pass = $post['pass'];
        $cp = $post['cp'];
        $fullname = $post['fullname'];
        $email = $post['email'];
        $phone = $post['phone'];

        $check = '/^\w{6,20}$/';
        $checkcp = '/^([a-zA-Z])\w{7,14}$/';
        $checkfn = '/^[a-zA-Z]$/';
        $regex_phone = '/((09|03|07|08|05)+([0-9]{8})\b)/';
        $regex_email = '/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/';

        $sql = "SELECT * FROM  $this->customer WHERE name = '$name'";
        $res = $this->Query($sql);
        $sqle = "SELECT * FROM  $this->customer WHERE email = '$email'";    
        $rese = $this->Query($sqle);
        if (!preg_match($check, $name)) {
            echo "<div class='container'>"."<p style='margin-left: 100px; background: aqua;padding: 5px;' class=\"text-success\">Tên tài khoản gồm ký tự từ a-z hoặc A-Z hoặc 0-9 tối đa 6 - 20 ký tự</p>"."</div>";
        } else if (!preg_match($checkcp, $cp)) {
            echo "<div class='container'>"."<p style='margin-left: 100px; background: aqua;padding: 5px;' class=\"text-success\">Mật khẩu từ 8 đến 14 ký tự</p>"."</div>";
        } else if (!preg_match($regex_email, $email)) {
            echo "<div class='container'>"."<p style='margin-left: 100px; background: aqua;padding: 5px;' class=\"text-success\">Email sai định dạng</p>"."</div>";
        } else if ($res->num_rows == 1) {
            echo "<div class='container'>"."<p style='margin-left: 100px; background: aqua;padding: 5px;' class=\"text-success\">Tên tài khoản bị đã tồn tại</p>"."</div>";
        } else if ($rese->num_rows == 1) {
            echo "<div class='container'>"."<p style='margin-left: 100px; background: aqua;padding: 5px;' class=\"text-success\">Email đã được đăng ký</p>"."</div>";
        } else if ($pass != $cp) {
            echo "<div class='container'>"."<p style='margin-left: 100px; background: aqua;padding: 5px;' class=\"text-success\">Mật khẩu nhập lại sai</p>"."</div>";
        } else if (!preg_match($regex_phone, $phone)) {
            echo "<div class='container'>"."<p style='margin-left: 100px; background: aqua;padding: 5px;' class=\"text-success\">Số điện thoại sai định dạng</p>"."</div>";
        } else if (!preg_match($checkfn, $fullname)) {
            $pass = password_hash($cp, PASSWORD_DEFAULT);
            $sql = "INSERT INTO $this->customer VALUES ('', '$name', '$pass', '$fullname', '$email', '$phone')";
            $res = $this->Query($sql);
            if ($res) {
                echo "<div class='container'>"."<p style='margin-left: 100px; background: aqua;padding: 5px;' class=\"text-success\">Đăng ký thành công...!</p>"."</div>";
            }
        }
    }

    public function loadLogin($name)
    {
        $sql = "SELECT * FROM  $this->customer WHERE name = '{$name}'";
        $res = $this->Query($sql);
        if ($res->num_rows == 1) {
            $user = $res->fetch_assoc();
            return $user;
        }
        return null;
    }


}
