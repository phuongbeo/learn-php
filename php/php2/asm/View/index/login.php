<div class="container">
    <div class="row">
        <div class="col-sm-5 col-sm-offset-4" style="position: relative;
		  left: 50%; top: 30px;
		  margin-left: -210px;">
            <div class="card">
                <div class="card-body">
                    <h1 class="text-center text-info">Login</h1>
                    <form action="" enctype="multipart/form-data" method="post" role="form"
                          onsubmit="return validateForm();">
                        <?php
                        if (!empty($this->dataView['err'])) {
                            echo '<p class="err">' . implode('<br>', $this->dataView['err']) . '</p>';
                        }
                        if (!empty($this->dataView['msg'])) {
                            echo '<p class="msg">' . implode('<br>', $this->dataView['msg']) . '</p>';
                        }

                        ?>
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" class="form-control" name="name" placeholder="Enter username">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" name="pass" placeholder="Enter passwoord">
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-danger">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script language="javascript">
    function validateForm() {
        var i;
        var control = document.getElementsByClassName('form-control');
        var length = document.getElementsByClassName('form-control').length;

        for (i = 0; i < length; i++) {
            var data = control[i].value;
            if (data == '') {
                alert('Bạn cần nhập đầy đủ giá trị!');
                return false;
            }
        }

    }
</script>
