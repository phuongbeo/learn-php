<?php
$objUserModel = new UserModel();
$list = $objUserModel->Category();
$showProduct = $objUserModel->loadProduct();
if (!empty($_GET["action"])) {
    switch ($_GET["action"]) {
        case "add":
            if (!empty($_POST["quantity"])) {
                $productByCode = "SELECT * FROM tblproduct WHERE code='" . $_GET["code"] . "'";
                $itemArray = array($productByCode[0]["code"] => array('name' => $productByCode[0]["name"],
                    'code' => $productByCode[0]["code"],
                    'quantity' => $_POST["quantity"],
                    'price' => $productByCode[0]["price"],
                    'image' => $productByCode[0]["image"]));

                if (!empty($_SESSION["cart_item"])) {
                    if (in_array($productByCode[0]["code"], array_keys($_SESSION["cart_item"]))) {
                        foreach ($_SESSION["cart_item"] as $k => $v) {
                            if ($productByCode[0]["code"] == $k) {
                                if (empty($_SESSION["cart_item"][$k]["quantity"])) {
                                    $_SESSION["cart_item"][$k]["quantity"] = 0;
                                }
                                $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                            }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }
            }
            break;
        case "remove":
            if (!empty($_SESSION["cart_item"])) {
                foreach ($_SESSION["cart_item"] as $k => $v) {
                    if ($_GET["code"] == $k)
                        unset($_SESSION["cart_item"][$k]);
                    if (empty($_SESSION["cart_item"]))
                        unset($_SESSION["cart_item"]);
                }
            }
            break;
        case "empty":
            unset($_SESSION["cart_item"]);
            break;
    }
}
?>
<div class="container">
        <div id="shopping-cart">
            <div class="txt-heading">Shopping Cart</div>

            <a id="btnEmpty" href="?act=showProduct&action=empty">Empty Cart</a>
            <?php
            if (isset($_SESSION["cart_item"])) {
                $total_quantity = 0;
                $total_price = 0;
                ?>
                <table class="tbl-cart" cellpadding="10" cellspacing="1">
                    <tbody>
                    <tr>
                        <th style="text-align:left;">Name</th>
                        <th style="text-align:left;">Code</th>
                        <th style="text-align:right;" width="5%">Quantity</th>
                        <th style="text-align:right;" width="10%">Unit Price</th>
                        <th style="text-align:right;" width="10%">Price</th>
                        <th style="text-align:center;" width="5%">Remove</th>
                    </tr>
                    <?php
                    foreach ($_SESSION["cart_item"] as $item) {
                        $item_price = $item["quantity"] * $item["price"];
                        ?>
                        <tr>
                            <td><img src="<?php echo $item["image"]; ?>" style="width: 50px;height: 50px;"
                                     class="cart-item-image"/><?php echo $item["name"]; ?>
                            </td>
                            <td><?php echo $item["code"]; ?></td>
                            <td style="text-align:right;"><?php echo $item["quantity"]; ?></td>
                            <td style="text-align:right;"><?php echo "$ " . $item["price"]; ?></td>
                            <td style="text-align:right;"><?php echo "$ " . number_format($item_price, 2); ?></td>
                            <td style="text-align:center;"><a
                                        href="?act=showProduct&action=remove&code=<?php echo $item["code"]; ?>"
                                        class="btnRemoveAction"><img src="Public/img/icon-delete.png"
                                                                     alt="Remove Item"/></a></td>
                        </tr>
                        <?php
                        $total_quantity += $item["quantity"];
                        $total_price += ($item["price"] * $item["quantity"]);
                    }
                    ?>

                    <tr>
                        <td colspan="2" align="right">Total:</td>
                        <td align="right"><?php echo $total_quantity; ?></td>
                        <td align="right" colspan="2"><strong><?php echo "$ " . number_format($total_price, 2); ?></strong>
                        </td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <?php
            } else {
                ?>
                <div class="no-records">Your Cart is Empty</div>
                <?php
            }
            ?>
        </div>
    <div id="product-grid">
        <div class="txt-heading">Products</div>
        <div class="row">
            <div class="col-sm-10">
                <?php

                foreach ($showProduct as $key => $value) {
                    ?>
                    <div class="product-item">
                        <form method="post" action="?act=showProduct&action=add&code=<?php echo $value["code"]; ?>">
                            <div class="product-image mt-2"><img src="<?php echo $value["image"]; ?>"></div>
                            <div class="product-tile-footer mb-2">
                                <div class="text-center my-2"><?php echo $value["name"]; ?></div>
                                <div class="product-price"><?php echo number_format($value["price"]) . "<sup>" . "đ" . "</sup>"; ?></div>
                                <div class="cart-action">
                                    <input style="width: 50px" type="text" class="product-quantity" name="quantity" value="1" size="2"/>
                                    <input type="submit" value="Add cart" class="btnAddAction"/></div>
                            </div>
                        </form>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="col-sm-2">
                <label for="smartphone">Điện thoại</label>
                <ul class="list-group">
                    <?php foreach ($list as $u): ?>
                        <a style="text-decoration: none; color: black;" href="?act=showProduct&id=<?php echo $u['id']; ?>&category=<?php echo $u['category_name']; ?>" class="list-group-item"><b><?php echo $u['category_name'] ?></b></a>
                    <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
