<?php
$objUserModel = new UserModel();
$data = $objUserModel->Slide();
$list = $objUserModel->Category();
?>
<div class="container">
    <div class="row">
        <div class="col-sm-10">
            <label for="smartphone">Slide Show</label>
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">


                    <?php foreach ($data as $key => $u): ?>
                        <div class="carousel-item <?php echo $key == 0 ? 'active' : '' ?>">
                            <img src="<?php echo $u['image'] ?>" title="<?php echo $u['title'] ?>"
                                 style="width:100%; height: 500px;">
                        </div>
                    <?php endforeach ?>
                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
        <div class="col-sm-2">
            <label for="smartphone">Điện thoại</label>
            <ul class="list-group">
                <?php foreach ($list as $u): ?>
                    <a style="text-decoration: none; color: black;"
                       href="?act=showProduct&id=<?php echo $u['id']; ?>&category=<?php echo $u['category_name']; ?> "
                       class="list-group-item"><b><?php echo $u['category_name'] ?></b></a>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</div>
