<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Assignment</title>
    <link rel="stylesheet" href="Public/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style type="text/css">
        .err {
            padding: 10px;
            background: yellow;
            color: red;
        }

        .msg {
            padding: 10px;
            background: cyan;
            color: blue;
        }
    </style>
</head>
<body>
<div class="container">
    <?php
    if (isset($_SESSION['auth']) && $_GET['act'] !== 'login' && $_GET['act'] !== 'regis') {
        echo 'Xin chào: ' . '<b>'. $_SESSION['auth']['name'] . '</b>';
        echo '<br><a class="btn btn-primary mt-1" href="' . base_path . '?act=logout">Thoát</a>';
        echo "<hr>";
    } else {
        echo '<br><a class="btn btn-danger my-2 mx-2" href="' . base_path . '?act=view">Home</a>';
        echo '<a class="btn btn-warning my-2 mx-2" href="' . base_path . '?act=login">Login</a>';
        echo '<a class="btn btn-primary my-2 mx-2" href="' . base_path . '?act=regis">Register</a>';
        echo "<hr>";
    }
    ?>
</div>
<?php $this->showContent(); ?>
</body>
</html>

