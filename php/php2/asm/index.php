<?php
if (isset($_SERVER['HTTPS'])) {
    $url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header("Location: " . $url);
    exit();
}
session_start();
date_default_timezone_set('Asia/Ho_Chi_Minh');
define('app_path', __DIR__);
define('base_path', '/php2/lab5');

require_once app_path . '/Core/App.php';
require_once app_path . '/Core/DB.php';
require_once app_path . '/Core/ControllerBase.php';
$app = new App();
$app->run();
?>