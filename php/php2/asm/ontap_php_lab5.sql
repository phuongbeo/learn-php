-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th3 08, 2020 lúc 12:21 PM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ontap_php_lab5`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `category_name`) VALUES
(1, 'Iphone'),
(2, 'SamSung'),
(3, 'Xiaomi'),
(4, 'Huawei'),
(5, 'Oppo');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `cusid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `fullname` varchar(165) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telephone` int(11) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`cusid`, `name`, `pass`, `fullname`, `email`, `telephone`, `role`) VALUES
(12, 'phuongntph08862', '$2y$10$RUTZrPY8Z6KXNrZfKWcoBuV6DNFyfuKJ0n4PUS5RNw7adTC6fhxKm', 'Phuong Nguyen', 'phuongntph08862@fpt.edu.vn', 974667645, 1),
(13, 'dungcao', '$2y$10$pH8yNUsVPkmPLsP5UqcvZ.tnJEQcqYemn9qoSZigQ4i4SwU6ImYDa', 'dung cao', 'dungcaondabc@gmail.com', 974667645, 0),
(14, 'nguyenbinh', '$2y$10$HM7PYp/ytEv/oFK72gsfTuh4JukxIQfjlOFnM4r7nilNAnxYwr7Eu', 'Nguyen Binh', 'nguyenbinh@gmail.com', 988695594, 0),
(15, 'imath2020', '$2y$10$259.jcaRTQBg/aD9.5BBhO68Az4zyQrDBxjy4TIVunYr.8E6EVOQq', 'imath imath', 'imath@gmail.com', 974667645, 0),
(16, 'nguyetcao', '$2y$10$ZCSUEecqs6MCKN7K/uyuquRGrhvTta08l0KpDj4A7X5A3JEWQX0pS', 'Cao Thi Nguyet', 'caothinguyet@gmail.com', 974667645, 0),
(17, 'phuongbeo', '$2y$10$KVVJoPE7.aaGB6KkFO5g.e0qwMW/M.siDXV5AzSU081vn87kJPuW2', 'Phuong Beo', 'phuongbeo@gmail.com', 974667645, 0),
(18, 'nguyenvana', '$2y$10$2go6Od67JZOaQxY25Rhitu8QTIE1MurVWW.W.vE4CfVtMTWqco4b2', 'nguyen van a', 'nguyenvana@gmail.com', 974667645, 0),
(19, 'nguyenvanb', '$2y$10$RLpTrGYCpB2TtFYfk/1uHOVqGZVmnf07NZ/OuX8ZxA/zPfBb0Faj.', 'nguyen van b', 'nguyenvanb@gmail.com', 974667645, 0),
(20, 'nguyenvanc', '$2y$10$Sl43Pj5y85SSkk/DwDZgq.5kI7LARW..ShTFAHLYorHEj.xruys5a', 'nguyen van c', 'nguyenvanc@gmail.com', 974667645, 0),
(21, 'nguyenthanhphuong', '$2y$10$kmjUZPsH71.gSnvNePY.h.OzmBMIZ/JgZxcnM77bvQrVyR2oJRz/e', 'nguyen thanh phuong', 'phuongntph08862@gmail.com', 974667645, 0),
(22, 'nguyenthia', '$2y$10$bQLyYgbV7zptTJaYlTjEme3WYqljcz4mzt5nI8R2VLrmqS/ytFT7q', 'nguyen thi a', 'nguyenthia@gmail.com', 974667645, 0),
(23, 'nguyenthib', '$2y$10$x3j1ggSkuwWLj9Q38zYyC.sRTUzPo5VGBho/dKOYhdeN7dqKEMKAy', 'nguyenthib', 'nguyenthib@gmail.com', 965894723, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `image`, `title`) VALUES
(1, 'product-images/slide1.jpg', 'slide show 1'),
(2, 'product-images/slide2.jpg', 'slide show 2'),
(3, 'product-images/slide3.jpg', 'slide show 3');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblproduct`
--

CREATE TABLE `tblproduct` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `price` int(11) NOT NULL,
  `id_category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `tblproduct`
--

INSERT INTO `tblproduct` (`id`, `name`, `code`, `image`, `price`, `id_category`) VALUES
(1, 'FinePix Pro2 3D Camera', '3DcAM01', 'product-images/camera.jpg', 1500, 0),
(2, 'EXP Portable Hard Drive', 'USB02', 'product-images/external-hard-drive.jpg', 800, 0),
(3, 'Luxury Ultra thin Wrist Watch', 'wristWear03', 'product-images/watch.jpg', 300, 0),
(4, 'XP 1155 Intel Core Laptop', 'LPN45', 'product-images/laptop.jpg', 800, 0),
(5, 'Iphone Pro 64GB', 'IP11', 'product-images/iphone11.jpg', 22000000, 1),
(7, 'Iphone X 64 GB', 'IPX', 'product-images/iphoneX_new.jpg', 12000000, 1),
(8, 'Samsung Galaxy S20', 'sss20', 'product-images/samsungS20.jpg', 21500000, 2),
(9, 'Iphone Xsmax 64GB', 'IPXsmax', 'product-images/iphoneXs.jpg', 20000000, 1),
(10, 'Iphone 8 32GB', 'IP8', 'product-images/iphone8.jpg', 17000000, 1),
(11, 'Iphone 7 256GB', 'IP7', 'product-images/iphone7.jpg', 15000000, 1),
(12, 'Iphone6 16GB', 'IP6', 'product-images/iphone6.jpg', 7000000, 1),
(13, 'Samsung Galaxy  S20+ G985', 'SS20PLUS', 'product-images/samsungS20plus.jpg', 24000000, 2),
(14, 'Samsung Galaxy Note 10 Lite', 'SS10', 'product-images/samsung-galaxy-note-10-lite.jpeg', 12500000, 2),
(15, 'Samsung Galaxy A51', 'SSA51', 'product-images/Samsung-Galaxy-A51.jpg', 7290000, 2),
(16, 'Samsung Galaxy A71', 'SSA71', 'product-images/Samsung-Galaxy-A71.jpg', 10900000, 2),
(17, 'Samsung Galaxy A30s', 'SSA30s', 'product-images/Samsung-Galaxy-A30s.png', 5190000, 2),
(18, 'Xiaomi Note 10 Pro', 'XM10', 'product-images/xiaomi-note-10-pro.jpg', 14000000, 3),
(19, 'Xiaomi Mi 9 Lite', 'XMm9', 'product-images/Xiaomi-Mi-9-Lite.jpg', 8500000, 3),
(20, 'Xiaomi Redmi Note 8 Pro', 'XMN8', 'product-images/xiaomi-redmi-note-8-pro.jpg', 5500000, 3),
(21, 'Xiaomi Redmi Note 7', 'XMN7', 'product-images/xiaomi-redmi-note-7.jpeg', 4290000, 3),
(22, 'Xiaomi Mi A3', 'XMA3', 'product-images/xiaomi-mi-a-3.jpg', 3790000, 3),
(23, 'Xiaomi Redmi 7A', 'XM7A', 'product-images/xiaomi-redmi-7a.jpg', 2500000, 3),
(24, 'Huawe Mate 30 Pro', 'HWM30', 'product-images/huawe_mate30_pro.jpg', 20990000, 4),
(25, 'Huawei Nova 5T', 'HW5T', 'product-images/huawei-nova-5t.jpeg', 8500000, 4),
(26, 'Huawei Nova 3I', 'HWN3I', 'product-images/huawei-nova-3i.jpg', 5990000, 4),
(27, 'Huawei Y9s', 'HWY9', 'product-images/huawei-Y9s.jpg', 5000000, 4),
(28, 'Huawei P30 Pro', 'HWP30', 'product-images/huawei-p30-pro.jpg', 5490000, 4),
(29, 'Huawei Y7 Pro', 'HWY7', 'product-images/Huawei-Y7-Pro.jpg', 3490000, 4),
(30, 'Oppo Reno 10x Zoom', 'OPR10', 'product-images/oppo-reno-10x-zoom.jpeg', 14690000, 5),
(31, 'Oppo Reno2 F', 'OPR2', 'product-images/OPPO-Reno2-f.jpg', 7990000, 5),
(32, 'Oppo A31', 'OPA31', 'product-images/oppo-a31.jpg', 4490000, 5),
(33, 'Oppo F11 Pro', 'OPF11', 'product-images/oppo-f11-pro.jpeg', 6490000, 5),
(34, 'Oppo A9', 'OPA9', 'product-images/oppo-a9.jpg', 5990000, 5),
(35, 'Oppo A5', 'OPA5', 'product-images/oppo-a5.jpeg', 4790000, 5);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`cusid`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tblproduct`
--
ALTER TABLE `tblproduct`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_code` (`code`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `cusid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `tblproduct`
--
ALTER TABLE `tblproduct`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
