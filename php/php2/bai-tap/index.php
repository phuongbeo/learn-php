<?php
$host = "127.0.0.1";
$dbname = "php2";
$dbusername = "root";
$dbpassword = "";
try{
    $connect = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $dbusername, $dbpassword);

}catch(Exception $ex){
    var_dump($ex->getMessage());
    die;
}

// câu query
$sql = "select * from users";
// nạp câu truy vấn vào kết nối
$stmt = $connect->prepare($sql);
// thực thi câu truy vấn với csdl
$stmt->execute();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Users</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Users</h3>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>email</th>
                                <th>
                                    username
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($stmt as $u): ?>
                                <tr>
                                    <td><?php echo $u['id'] ?></td>
                                    <td><?php echo $u['email'] ?></td>
                                    <td><?php echo $u['username'] ?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    <a href="dangky.php" class="btn btn-primary stretched-link">Đăng Ký</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>