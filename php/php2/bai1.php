<?php
class SinhVien{
    //khai báo thuộc tính
    public $maSV = 'SV1';
    //khai báo hằng nếu cần
    const ABC = 123;
    //khai báo phương thức
    public function In(){
        echo "<br>In Thông Tin ";
        echo $this->maSV;// chỉ sử dụng trong hàm từ khóa this
        //chúng ta có 2 cách truy cập
        echo '<br>';
        echo self::ABC;
    }
}
// Tạo đối tượng
$objSV = new SinhVien();
echo 'Ma SV: '. $objSV->maSV;
$objSV->maSV='002';// Gán giá trị
$objSV->In();
// sử dụng hằng
echo '<br> Hằng ABC = '.SinhVien::ABC;


?>