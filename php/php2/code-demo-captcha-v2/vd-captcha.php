<?php session_start();

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}



header ('Content-Type: image/png');
$anh = @imagecreatetruecolor(70, 40) or die('Loi khong tao duoc anh');

// tạo nền cho ảnh: 
$bgColor = imagecolorallocate($anh, 56, 237, 195);
imagefill($anh , 0,0 , $bgColor);





$text_color = imagecolorallocate($anh, 233, 14, 91);  
// ba con số ở trên là các chỉ số màu.
// tạo màu bằng rgb: https://www.w3schools.com/colors/colors_rgb.asp

$noi_dung = generateRandomString(5);

// ghi nội dung captcha vào session để lúc người dùng gửi lên sẽ đối chiếu có nhập đúng trong ảnh hay không. Nếu đúng thì cho làm các công việc tiếp theo. 
// Bước 1: Bật session ở đầu trang
// Bước 2: Ghi chuỗi noi_dung vào session VD:  $_SESSION['captcha'] = $noi_dung;
// Bước 3: Ở trang mà người dùng post form lên, kiểm tra chuỗi người dùng nhập đối chiếu với trong session.
$_SESSION['captcha'] = $noi_dung;


imagestring($anh, 5, 10, 10, $noi_dung , $text_color);
// hàm imagestring là: viết chữ lên ảnh.

 
imagepng($anh); // đẩy ảnh xuống trình duyệt
imagedestroy($anh); // giải phóng bộ nhớ.