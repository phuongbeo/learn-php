<?php
 if(!isset($_GET['abc']))
 	die('khong co quyen');


$chuoi = base64_encode(file_get_contents('giay.jpg'));
// echo $chuoi;

$noi_dung_anh = base64_decode($chuoi);

// echo $noi_dung_anh;

$anh = imagecreatefromstring($noi_dung_anh);

// echo "<br>Chieu rong anh: ". imagesx($anh);

// đẩy ảnh về trình duyệt:
// yêu cầu: trước lệnh đẩy ảnh về trình duyệt không có lệnh echo hoặc thẻ html.
header('Content-Type: image/jpeg'); 
 // tra cứu thêm để biết các loại Content-type:  https://www.php.net/manual/en/function.mime-content-type.php#87856
// gọi hàm tạo ảnh
imagejpeg($anh);  // gửi xuống trình duyệt


// trong trường hợp muốn ghi ảnh vào file trên server thì sẽ không cần lệnh header ở trên mà dùng hàm imagexxx để ghi vào file luôn

imagejpeg($anh,__DIR__.'/anh_2.jpg');


imagedestroy($anh); // vì ảnh lưu ở bộ nhớ nên cần giải phóng. 
