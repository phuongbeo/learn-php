<?php

class TinhToan2So
{
    function __construct($_soA, $_soB)
    {
        $this->soA = $_soA;
        $this->soB = $_soB;
    }
}

class MayTinh extends TinhToan2So
{
    public function Sum()
    {
        $sum = $this->soA + $this->soB;
        echo $sum;
    }

    public function Sub()
    {
        $sub = $this->soA - $this->soB;
        echo $sub;
    }

    public function Mul()
    {
        $mul = $this->soA * $this->soB;
        echo $mul;
    }

    public function Dev()
    {
        $dev = $this->soA / $this->soB;
        echo round($dev, 1);
    }
}


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Máy Tính</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <div class="panel panel-primary">
                <div class="panel-heading">Máy Tính</div>
                <div class="panel-body">
                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="title">Nhập số a</label>
                                    <input type="number" name="so_a" placeholder="Nhập a = ..." class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="title">Nhập số b</label>
                                    <input type="number" name="so_b" placeholder="Nhập b = ..." class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="title">Chọn phép toán</label>
                                    <select name="pt" class="form-control">
                                        <option value="">Chọn phép toán</option>
                                        <option value="+">+</option>
                                        <option value="-">-</option>
                                        <option value="*">*</option>
                                        <option value="/">/</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="content">Thực hiện</label>
                                    <input type="submit" name="submit" value="Tính" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="number"
                                           value="<?php if (isset($_POST['submit'])) {
                                                        $objMT = new MayTinh($_POST['so_a'], $_POST['so_b']);
                                                        switch ($_POST['pt']) {
                                                            case '+':
                                                                $kq = $objMT->Sum();
                                                                break;
                                                            case '-':
                                                                $kq = $objMT->Sub();
                                                                break;
                                                            case '*':
                                                                $kq = $objMT->Mul();
                                                                break;
                                                            case '/':
                                                                $kq = $objMT->Dev();
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                    } ?>"
                                           class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.min.js"></script>
</body>
</html>