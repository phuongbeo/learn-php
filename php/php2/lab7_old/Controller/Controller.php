<?php
require_once '../Model/file_util.php';
$action = null;
if(isset($_GET["action"]))
    $action = $_GET["action"];
else if(isset($_POST["action"]))
    $action = $_POST["action"];
else
    $action = "viewImage";

$image_dir = 'images';
$image_dir_path = getcwd().DIRECTORY_SEPARATOR.$image_dir;

switch ($action){
    case "upload_form":
        include "../View/uploadFile.php";
        break;
    case "upload":
        if($_FILES["file1"]["error"] > 0){
            echo "Error: " . $_FILES["file1"]["error"] . "<br>";
        }else{
            echo "Upload: " . $_FILES["file1"]["name"] . "<br>";
            echo "Type: " . $_FILES["file1"]["type"] . "<br>";
            echo "Size: " . ($_FILES["file1"]["size"] / 1024) . " kB<br>";
            echo "Stored in: " . $_FILES["file1"]["tmp_name"];
        }
        if(isset($_FILES['file1'])){
            echo $filename = $_FILES["file1"]["name"];
            if(empty($filename))
                break;
            $source = $_FILES["file1"]["tmp_name"];
            $target =  $image_dir_path.DIRECTORY_SEPARATOR. getName();
            move_uploaded_file($source,$target);
        }
    case "upload_multi_form":
        include "../View/uploadMultiFile.php";
    case "uploadmulti":
        if($_FILES["file1"]["error"][0] > 0){
            echo "Error: " . $_FILES["file1"]["error"][0] . "<br>";
        }else if ($_FILES["file1"]["error"][1] > 0){
            echo "Error: " . $_FILES["file1"]["error"][1] . "<br>";
        }else{
            echo "<h1>Image 1</h1>";
            echo "Upload: " . $_FILES["file1"]["name"][0] . "<br>";
            echo "Type: " . $_FILES["file1"]["type"][0] . "<br>";
            echo "Size: " . ($_FILES["file1"]["size"][0] / 1024) . " kB<br>";
            echo "Stored in: " . $_FILES["file1"]["tmp_name"][0];
            echo "<h1>Image 2</h1>>";
            echo "Upload: " . $_FILES["file1"]["name"][1] . "<br>";
            echo "Type: " . $_FILES["file1"]["type"][1] . "<br>";
            echo "Size: " . ($_FILES["file1"]["size"][1] / 1024) . " kB<br>";
            echo "Stored in: " . $_FILES["file1"]["tmp_name"][1];
        }
        if(isset($_FILES["file1"])){
            echo $filename1 = $_FILES["file1"]["name"][0];
            echo $filename2 = $_FILES["file1"]["name"][1];
            if(empty($filename1))
                break;
            if(empty($filename2))
                break;
            $source = $_FILES['file1']['tmp_name'][0];
            $target = $image_dir_path.DIRECTORY_SEPARATOR. $filename1;
            move_uploaded_file($source,$target);
            $source = $_FILES['file1']['tmp_name'][1];
            $target = $image_dir_path.DIRECTORY_SEPARATOR. $filename2;
            move_uploaded_file($source,$target);
        }
        break;
    case "viewImage":
        $files = get_file_list($image_dir_path);
        include "../View/viewImage.php";
        break;
}

