<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="container">
    <div id="upload">
        <h2>Image to be uploaded</h2>
        <form id="upload_form" action="../Controller/Controller.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="action" value="upload">
            <input type="file" name="file1" id="file1">
            <input type="submit" id="upload_button" value="Upload">
        </form>
    </div>
</div>
</body>
</html>