<?php
require_once "../Model/file_util.php";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>File PHP</title>
    <link rel="stylesheet" href="../Public/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="main">
        <h2>Image in directory</h2>
        <a class="btn btn-primary" href="?action=upload_form">Upload image</a>  <a class="btn btn-info" href="?action=upload_multi_form">Upload multi file</a>
        <br>
        <?php if (count($files) == 0): ?>
            <p>No image uploaded.</p>
        <?php else: ?>
            <ul style="list-style-type: none">
                <?php
                foreach ($files as $filename):
                    $file_url = $image_dir . DIRECTORY_SEPARATOR . $filename;
                    ?>
                    <li style="float: left">
                        <a href="<?php echo $file_url; ?>">
                            <img src="image/<?php echo $filename; ?>" alt="" width="300px" height="180px">
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>
<script type="text/javascript" src="../Public/js/jquery.min.js"></script>
<script type="text/javascript" src="../Public/js/bootstrap.min.js"></script>
</body>
</html>