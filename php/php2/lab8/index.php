<?php
session_start();
$host = "http://learnphp.local/php2/lab8/";
$error = array();
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (!empty($_SESSION['captcha'])) {
        // kiểm tra dữ liệu post:
        $nameCaptcha = isset($_POST['name_captcha']) ? $_POST['name_captcha'] : '';
        if (!$nameCaptcha) {
            $error['captcha'] = "Vui long nhap captcha";
        } elseif ($nameCaptcha != $_SESSION['captcha']) {
            $error['captcha'] = "Sai captcha, vui long nhap lai";
//        echo "Captcha OK, tiep tuc lam cac cong viec khac";
            //...... các công việc tiếp theo ở khu vực này
        } else {
            $target_dir = "uploads/";
            $nameImage = basename($_FILES['file']['name']);
            $target_file = $target_dir . $nameImage;
            $file_link = $host . $target_file;
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $ext = strtolower($ext);

            $file = $_FILES['file']['tmp_name'];

            if (file_exists($target_file)) unlink($target_file);

            if (in_array($ext, ['gif', 'png', 'jpg'])) {
                $properties = getimagesize($file);
                $width = $properties[0];
                $height = $properties[1];
                $imageType = $properties[2];
                $targetWidth = $width;
                $targetHeight = $height;
                if($width > 800) $targetWidth = 800;
                if($height > 600) $targetHeight = 600;

                switch ($imageType) {
                    case IMAGETYPE_PNG:
                        $imageResourceId = imagecreatefrompng($file);
                        $targetLayer = imageResize($imageResourceId,$width,$height,$targetWidth,$targetHeight);
                        imagepng($targetLayer, $target_file);
                        $check = true;
                        break;
                    case IMAGETYPE_GIF:
                        $imageResourceId = imagecreatefromgif($file);
                        $targetLayer = imageResize($imageResourceId,$width,$height,$targetWidth,$targetHeight);
                        imagegif($targetLayer, $target_file);
                        $check = true;
                        break;
                    case IMAGETYPE_JPEG:
                        $imageResourceId = imagecreatefromjpeg($file);
                        $targetLayer = imageResize($imageResourceId,$width,$height,$targetWidth,$targetHeight);
                        imagejpeg($targetLayer, $target_file);
                        $check = true;
                        break;
                    default:
                        break;
                }
            } else {
                if (move_uploaded_file($file, $target_file)) {
                    $check = true;
                }
            }
        }
    } else {
        echo "Loi: Chưa có session captcha";
    }
}


function imageResize($imageResourceId,$width,$height,$targetWidth,$targetHeight) {
    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);

    return $targetLayer;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Upload file and captcha</title>
    <link rel="stylesheet" href="Public/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row" style="margin-top: 50px;">
        <div class="col-md-4">
            <form action="" id="form_upload" method="POST" role="form" enctype="multipart/form-data">
                <legend>Upload File</legend>
                <div class="form-group">
                    <label for="">Chọn file:</label>
                    <input type="file" class="form-control" name="file" id="file" required>
                </div>
                <div class="form-group">
                    <label for="">Nhập captcha:</label>
                    <input type="text" class="form-control" name="name_captcha" id="name_captcha" placeholder="Nhập captcha..." required autocomplete="off">
                    <?php
                    if (isset($error['captcha'])) {
                        ?>
                        <p style="color: red"><?php echo $error['captcha']; ?></p>
                        <?php
                    }
                    ?>
                </div>
                <div class="form-group">
                    <img src="<?php echo $host ?>set_captcha.php">
                </div>
                <button type="submit" class="btn btn-primary">Upload</button>
                <button type="reset" class="btn btn-info">Reset</button>
            </form>
            <?php if (isset($check)) {
                echo "<p style='color: aqua; margin-top: 20px;'>Bạn đã upload thành công...!</p>";
            }
            ?>
        </div>
        <div class="col-md-8">
            <?php
            if (isset($check)) {
                ?>
                <img src="<?php echo $target_file; ?>" alt="">
                <?php
                echo "<p style=\"margin-top: 20px;\">Link file upload: <a href=\"$file_link\" target=\"_blank\">$file_link</a></p>";
            }
            ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="Public/js/jquery.min.js"></script>
<script type="text/javascript" src="Public/js/bootstrap.min.js"></script>
</body>
</html>



