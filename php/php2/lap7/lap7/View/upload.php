<!DOCTYPE html>
<html>
<head>
	<title>PHP file</title>
	<meta charset="utf-8">
</head>
<body>
	<div id="main">
		<h2>Image in directory</h2>
		<a href="?action=upload_form">Upload Image</a> | <a href="?action=upload_multi_form">Upload multi file</a>
		<br/>
		<?php if (count($files)==0):?> 
		<p>No image upload</p>	
		<?php else:?>
			<ul style="list-style-type: none">
              <?php
                 foreach ($file as $filename): 
                 	$file_url = $image_dir.DIRECTORY_SEPARATOR.$filename;
              ?>
              <li style="float: left;">
              	<a href="<?php echo $file_url;?>">
              		<img src="image/<?php echo $filename;?>" with="300px" height="180px">
              	</a>
              </li>
          <?php endforeach ;?>
			 </ul>
			<?php endif;?>
	</div>

</body>
</html>