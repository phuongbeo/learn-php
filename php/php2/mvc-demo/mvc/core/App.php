<?php
class App{
    // http://learnphp.local:88/php2/mvc-demo/Home/SayHi/1/2/3
    protected $controller="Home";
    protected $action="SayHi";
    protected $params=[];

    function __construct(){
        // Array ( [0] => Home [1] => SayHi [2] => 1 [3] => 2 [4] => 3 )
        $arr = $this->urlProcess();

        // xu ly controllers
        if(file_exists("./mvc/controllers/".$arr[0].".php")){
            $this->controller = $arr[0];
            unset($arr[0]);
        }
        require_once "./mvc/controllers/".$this->controller.".php"; // mac dinh hien thi controller-Home
        // xu ly action
        if(isset($arr[1])){
            if(method_exists($this->controller , $arr[1])){
                $this->controller = $arr[1];
            }
            unset($arr[1]);
        }
        // xu ly params
        $this->params = $arr?array_values($arr):[];
        call_user_func_array([$this->controller,$this->action],$this->params);
//        echo $this->controller;
//        echo $this->action;
//        print_r($this->params);
    }

    function urlProcess(){
        // Home/SayHi/1/2/3
        if( isset($_GET['url']) ){
            return explode("/", filter_var(trim($_GET['url'], "/")));
        }
    }
}
?>