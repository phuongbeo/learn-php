<?php
require_once app_path . '/Model/UserModel.php';
require_once app_path . '/Validations/Validate.php';

class IndexController extends ControllerBase
{
    public function Index()
    {
//        return $this;
        echo "Chào mừng đến với trang web của Phương Béo";
        echo '<a href="' . base_path . '/?ct=index&act=login"><button style="font-size:16px;background:lightblue;padding:10px 10px;border:none;border-radius:6px;margin-left: 10px" >Click Login</button></a>';
        echo '<a href="' . base_path . '/?ct=index&act=register"><button style="font-size:16px;background:lightcoral;padding:10px 10px;border:none;border-radius:6px;margin-left: 10px" >Click Register</button></a>';
    }

    public function Login()
    {
        $data = ['err' => [], 'msg' => []];
        if (isset($_POST['uname'])) {
// sự kiện post, xử lý công việc đăng nhập
// print_r($_POST);
            $username = $_POST['uname'];
            $passwd = $_POST['pwd'];
//1. Kiểm tra hợp lệ dữ liệu post
// Tự làm
            if (empty($username)) {
                $data['err'][] = 'Bạn phải nhập username!';
            }
            if (empty($passwd)) {
                $data['err'][] = 'Bạn phải nhập password!';
            }
//2. Kiểm tra tồn tại DB
            $objModel = new UserModel();
            $userInfo = $objModel->loadLogin($username, $passwd);
            if (!empty($userInfo)) {
//                if ($userInfo['passwd'] == $passwd) {
                if (password_verify($passwd, $userInfo['passwd'])) {
// đúng pass ==> cho đăng nhập
//4. Ghi session & chuyển về trang chủ
                    unset($userInfo['passwd']);

                    // Load danh sách các quyền được cấp
                    $pmsList = $objModel->loadPmsByRole($userInfo['id_role']);
// gán thêm vào mảng UserInfo để sử dụng ở hàm CHeckACL
                    $userInfo['list_pms'] = $pmsList;

                    $_SESSION['auth'] = $userInfo;
// đăng nhập thành công, chuyển trang
                    header('Location:' . base_path);
                } else {
// sai pass:
                    $data['err'][] = 'Sai password!';
                }   
// lấy được thông tin của tài khoản:
//3. Kiểm tra đúng pass
//4. Ghi session & chuyển về trang chủ
                unset($userInfo['passwd']);
                $_SESSION['auth'] = $userInfo;
// đăng nhập thành công, chuyển trang
                header('Location:' . base_path . '/?ct=user&act=list-all');
            } else {
// username không đúng
                $data['err'][] = 'Không tồn tại tài khoản ' . $username;
            }
        }
        $this->RenderView('index.login', $data);
    }

    public function Logout()
    {
        if (!empty($_SESSION['auth'])) {
            session_unset();
        }
        header('Location:' . base_path . '/?ct=user&act=list-all');
    }

    public function Register()
    {

        // Kiểm tra trạng thái đăng nhập, nếu đăng nhập rồi thì chuyển về trang chủ
        if (!empty($_SESSION['auth'])) {
            header('Location: ' . base_path);
        }


        $data = ['err' => [], 'msg' => []];

        $data_post = $_POST;
        // echo '<pre>';
        // print_r($data_post);
        // echo '</pre>';

        if (isset($_POST['fullname'])) {
            // khi người dùng thực hiện thao tác post dữ liệu lên(bấm nút ) thì mới kiểm tra, tránh cảnh báo lỗi khi mới vào giao diện đăng ký lần đầu

            $check = Validate::CheckRegister($data_post);

            if (empty($check)) {
                // không có lỗi ==> tiến hành xử lý mã hóa pass và ghi vào CSDL

                // $new_pass = sha1($data_post['passwd']);


                // echo '<br>Truoc khi ma hoa: '.$data_post['passwd'];

                // bước 1: Mã hóa password và lấy chuỗi này để lưu vào CSDL
                $new_pass_hash = password_hash($data_post['pwd'], PASSWORD_DEFAULT);

                // echo "<br>Chuoi pass ma hoa: " . $new_pass_hash;

                // echo "<br>Khi đăng nhập,kiểm tra pass: ";
                // var_dump( password_verify($data_post['passwd'], $new_pass_hash) );

                $data_post['pwd'] = $new_pass_hash;

                // bước 2: Tạo đối tượng model --> gọi hàm ghi vào CSDL
                $objModel = new UserModel();
                $save = $objModel->SaveRegister($data_post);

                if ($save === true) {
                    $data['msg'][] = 'Đăng ký thành công';
                    // có thể là chuyển trang về trang chủ hoặc cho đăng nhập luôn

                    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {
                        $name = $_POST['uname'];
                        $email = $_POST['email'];
                        $fullname = $_POST['fullname'];
                        $succes = $objModel->SaveRegister($data_post);
                        if ($succes) {
                            $to = $email;
                            $content = "Chào bạn " . $fullname . "
				Chúc mừng bạn đã đăng kí thành công tài khoản của bạn như sau:
				Username: " . $name . "
				Email: " . $email . "
				Ngày: " . date('Y-m-d');
                            $from = 'phuongntph08862@fpt.edu.vn';

                            $sendMail = Send_email_via_smtp_gmail($subject, $to, $content, array(), array(), $from, 0);

                            if ($sendMail['code'] == 1) {
                                // gui mail thanh cong
                                $data['msg'] = $succes;
                                echo $data['msg'];

                            } else {
                                echo $sendMail['msg'];
                            }
                        }
                    }
                } else {
                    $data['err'][] = $save;
                }


            } else {
                // có lỗi vì kết quả check không rỗng ==> truyền câu thông báo lỗi ra view
                $data['err'] = array_merge($data['err'], $check);

            }
        }


        $this->RenderView('index.register', $data);
    }

}