<?php
require_once app_path . '/Model/UserModel.php'; // nhúng file model vào để làm việc
class UserController extends ControllerBase
{
    public function ListAll()
    {
        $objUserModel = new UserModel(); // tạo đối tượng model
        $data = $objUserModel->getAll(); //gọi hàm trong model để lấy danh sách
//        echo '<table border="1" width="700">';
//        foreach ($data as $row) {
//            echo "<tr>";
//            foreach ($row as $column) {
//                echo "<td>$column</td>";
//            }
//            echo "</tr>";
//        }
//        echo '</table>';
        $this->RenderView('user.list-all', $data);
// hàm này sẽ gọi file view theo cấu trúc là: user/list-all.phtml
    }
}