<?php

/**
 *
 */
class UserModel extends DB
{

    private $tb = "tb_user";

    public function getAll()
    {
        // hàm lấy toàn bộ danh sách
        $sql = "SELECT * FROM $this->tb";
        $res = $this->Query($sql); // hàm này được kế thừa từ lớp DB
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row;
        }
        return $data; // Hàm getAll này luôn trả về 1 biến mảng, nếu không có dữ liệu thì mảng này là rỗng.
    }

    public function loadLogin($username, $passwd)
    {
        $sql = "SELECT * FROM tb_user WHERE username = '$username' and passwd = '$passwd'";
        $res = $this->Query($sql);
        if ($res->num_rows == 1) {
            $user = $res->fetch_assoc();
            return $user;
        }
        return null;
    }

    public function loadPmsByRole($id_role)
    {
        $sql = "SELECT * FROM tb_pms
        INNER JOIN role_pms on tb_pms.id = role_pms.id_pms
        WHERE role_pms.id_role = {$id_role}";
        $res = $this->Query($sql);
        $data = [];
        while ($row = $res->fetch_assoc()) {
            $data[] = $row['name'];
        }
// mảng các quyền được cấp chỉ cần lấy tên quyền.
        return $data;
    }

    public function SaveRegister($data){
        // dùng cho khách tự do đăng ký nên gắng cứng vào code vai trò là khách: là số 3
        $sql = "INSERT INTO tb_user (fullname, username, passwd, email, id_role)
				VALUES ('{$data['fullname']}',
						'{$data['uname']}',
						'{$data['pwd']}',
						'{$data['email']}',
						3) ";

        $res = $this->Query($sql);

        if($this->cnn->errno){
            return "Loi ghi CSDL: ". $this->cnn->error;
        }

        return true;

    }
}

?>