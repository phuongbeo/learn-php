<?php
class Validate
{
    static public function CheckRegister($input = [])
    {
        $out = [];

        $bt_email = '/^[A-Za-z0-9_.]{6,32}@([a-zA-Z0-9]{2,12})(.[a-zA-Z]{2,12})+$/';
        if(!preg_match($bt_email, $input['email'])){
            $out [] = "Email: Cần nhập chuỗi tiếng Việt không có dấu và ký tự đặc biệt, có '@' và dài từ 10 đến 40 ký tự";
        }

        $bt_fullname = '/^[ a-zA-ZàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđÀÁẠẢÃÂẦẤẬẨẪĂẰẮẶẲẴÈÉẸẺẼÊỀẾỆỂỄÌÍỊỈĨÒÓỌỎÕÔỒỐỘỔỖƠỜỚỢỞỠÙÚỤỦŨƯỪỨỰỬỮỲÝỴỶỸĐD]{2,30}$/';
        if (!preg_match($bt_fullname, $input['fullname'])) {
            $out [] = "Fullname: Cần nhập chuỗi tiếng Việt có dấu hoặc không dấu từ 2 đến 30 ký tự";
        }

        $bt_username = '/^[a-zA-Z]{2,30}$/';
        if(!preg_match($bt_username, $input['uname'])){
            $out [] = "Username: Cần nhập chuỗi tiếng Việt không có dấu từ 2 đến 30 ký tự";
        }


        $bt_password = '/^[a-zA-Z0-9]{6,10}$/';
        if(!preg_match($bt_password, $input['pwd'])){
            $out [] = "Password: Không có dấu dài từ 6 đến 30 ký tự";
        }
        return $out;
    }
}