<?php 
$host = "127.0.0.1";
$dbname = "assignment_php1"; // tên database - assignment_php1
$dbroom_name = "root";
$dbhotel_id = "";
try{
	$connect = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $dbroom_name, $dbhotel_id);	
	
}catch(Exception $ex){
	var_dump($ex->getMessage());
	die;
}
// câu query
$sql = "select * from rooms";
// nạp câu truy vấn vào kết nối
$stmt = $connect->prepare($sql);
// thực thi câu truy vấn với csdl
$stmt->execute();
// thu thập kết quả trả về
$result = $stmt->fetchAll();
?>
<table>
	<thead>
		<tr>
			<th>STT</th>
			<th>Room name</th>
			<th>Price</th>
			<th>Image</th>
			<th>Detail</th>
			<th>Status</th>
			<th>
				<a href="add-room.php" title="">Add new</a>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($result as $u): ?>
			<tr>
				<td><?php echo $u['id'] ?></td>
				<td><?php echo $u['room_name'] ?></td>
				<td><?php echo $u['price'] ?></td>	
				<td>
					<img src="<?php echo $u['image'] ?>" width="100">
				</td>
				<td><?php echo $u['detail'] ?></td>
				<td><?php echo $u['status'] ?></td>
				<td>
					<a href="edit-room.php?id=<?php echo $u['id']?>" title="">Edit</a>
					<a href="remove-room.php?id=<?php echo $u['id']?>" title="">Remove</a>
				</td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
