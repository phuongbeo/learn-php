<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Youtube extends Controller
{
    //
    public function index($name){
        return view('youtube', ['data'=>$name]);
    }

    public function index1(Request $request){
        print_r($request->input('name'));
    }

    public function template(){
        $data = [
            'name' => 'phuong',
            'age' => 21,
            'sex' => 'male',
            'job' => 'student',
            'dark' => '<b>Phuong Nguyễn</b>'];
        return view('template', ['items'=>$data]);
    }
}

