<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class loginController extends Controller
{
    //
    public function index(){
        return view('login');
    }

    public function index1(Request $request){
        $request->validate([
            'user' => 'required|min:8',
            'password' => 'required|min:6|max:8',
            'age' => 'required'
        ]);
//        print_r($request->input());
        $request->session()->flash('user', $request->input('user'));
        return view('welcome');
    }
}
