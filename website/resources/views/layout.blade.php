<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>app title - @yield('title')</title>
</head>
<body>
    <div style="background-color: blue">
        @section('sidebar')
        @show
    </div>
    <div class="main">
        @yield('main')
    </div>
    <div class="footer">
        @section('footer')
        @show
    </div>
</body>
</html>