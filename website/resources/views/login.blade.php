<!DOCTYPE html>
<html lang="en">
<head>
    <title>Laravel login middleware</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Login</h2>
    <form action="/post-login" method="post">
        @csrf
        <div class="form-group">
            <label for="user">User:</label>
            <input type="text" class="form-control" id="user" placeholder="Enter user" name="user">
        </div>
        <div class="form-group">
            <label for="password">Password:</label>
            <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
        </div>
        <div class="form-group">
            <label for="age">Age:</label>
            <input type="number" class="form-control" id="age" placeholder="Enter age" name="age">
        </div>
        <div class="form-group">
            @foreach($errors->all() as $error)
                <li style="color: red">{{$error}}</li>
            @endforeach
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
    </form>
</div>

</body>
</html>
