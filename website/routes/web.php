<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/user/{name?}', function ($name="default") { // simple routing
    return $name;
});

Route::get('/site/{name}', 'Youtube@index'); // simple view

Route::get('/site', 'Youtube@index1'); // http request

Route::get('/login', 'loginController@index');        //submit form and validate and flash session
Route::post('/post-login', 'loginController@index1')->middleware('age'); //submit form and validate and flash session

Route::get('/blade', 'Youtube@template'); // blade template

Route::view('/page', 'page');

